trigger CaseFileTrigger on Case_File__c( after insert, after update ) {
	CaseFileHelper.triggerAction( trigger.isAfter, trigger.isInsert, trigger.isUpdate
					, trigger.New, trigger.OldMap );

}