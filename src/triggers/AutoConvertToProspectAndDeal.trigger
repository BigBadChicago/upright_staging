trigger AutoConvertToProspectAndDeal on Lead (after insert) {
    
    List<Prospect__c> prosps_to_create = new List<Prospect__c>();
    List<Prospect__c> prosps_to_create_with_calls = new List<Prospect__c>();
    List<Prospect__c> prosps_to_create_with_deals = new List<Prospect__c>();
    List<Deal__c> deals_to_create = new List<Deal__c>();
    List<Call__c> calls_to_create = new List<Call__c>();
    List<Prospect__c> prosps_to_update = new List<Prospect__c>();
    
    Map<Id, Map<String, String>> lead_fields_for_deal = 
        new Map<Id, Map<String, String>>();
    
    if(lead.LeadSource != null){    
        for (Lead lead: Trigger.new) {
            
            
            // <------  BEGIN VELOCIFY LOGIC ------>    
            
            if (lead.LeadSource == 'Velocify_Prospect') {
                
                Prospect__c p = new Prospect__c(); 
                p.Name = lead.FirstName + ' ' + lead.LastName;
                p.First_Name__c = lead.FirstName; 
                p.Middle_Name__c = lead.Middle_Name__c; 			
                p.Last_Name__c = lead.LastName; 
                p.Personal_Email__c = lead.Email;
                p.Mobile_SMS_Phone__c = lead.ext_phone_3__c; 
                p.Home_Phone__c = lead.Phone;
                p.Work_Phone__c = lead.ext_phone_2__c; 
                p.Street__c = lead.Street; 
                p.Street_2__c = lead.ext_address_2__c;
                p.City__c = lead.City; 
                p.State__c = lead.State; 
                p.Postal_Code__c = lead.PostalCode;
                p.County__c = lead.County__c;
                p.Lead__c = lead.Id;
                p.Lead_Source__c = lead.LeadSource;
                
                prosps_to_create_with_deals.add(p); 
                
                Map<String, String> fields_for_deal = new Map<String, String>();
                fields_for_deal.put('motivator', lead.ext_motivator__c);
                fields_for_deal.put('hope', lead.ext_hope__c);
                fields_for_deal.put('pain_point', lead.ext_pain_point__c);
                fields_for_deal.put('facts', lead.ext_facts_about_debt__c);
                fields_for_deal.put('lead_campaign', lead.Lead_Campaign__c);
                fields_for_deal.put('velocifyID', lead.Velocify_ID__c);
                
                lead_fields_for_deal.put(lead.Id, fields_for_deal);
                
            }
            // <------  END VELOCIFY LOGIC ------>
            
            
            // <------  BEGIN BOBERDOO LOGIC ------>
            //Direct posts from Boberdoo started 11/30/15
            //Code checks for dupe Case files or Prospects and
            //Creates:
            // - Support Ticket for existing Case Files
            // - Email to Deal Owner for Prospects w/ open Deals
            // - Call record 'Scheduled' for 'now' for Prospects without Deals 
            
            else if (lead.LeadSource == 'boberdoo'){
                
                //Check for existing Case Files for this person
                List<Case_File__c> existingCaseFiles =  
                    AutoConvertHelper.checkForCaseFiles(
                        lead.LastName, 
                        lead.Email, 
                        lead.Phone ,
                        lead.PostalCode, 
                        lead.State);
                
                //If Case File(s) exist(s)
                if(existingCaseFiles.size() > 0){
                    
                    //Create Support Ticket
                    Case_File__c cf = existingCaseFiles[0];
                    AutoConvertHelper.createSupportTicket(
                        cf.Id,
                        cf.Primary_Contact_ID__c, 
                        cf.Primary_Contact_Email__c);
                }
                else{ //If no existing Case Files
                    
                    //Check for existing Prospect records (i.e. email and last  
                    //name match AND lead.phone = any Prospect phone field):
                    List<Prospect__c> dupeProspects = 
                        AutoConvertHelper.checkForDupeProspects(
                            lead.LastName, 
                            lead.email, 
                            lead.Phone);
                    
                    //If duplicate Prospect record exists:
                    if(dupeProspects.size() > 0){ 
                        
                        List<Id> dupeIds = new List<Id>();
                        
                        for (Prospect__c p : dupeProspects) {
                            
                            //Add each Prospect__c.Id to a Set that will be used 
                            //to check if there are Deal or Call records on 
                            //any of them
                            dupeIds.add(p.Id);
                            
                        }
                        
                        //Check for open Deals
                        List<Deal__c> existingDeals = 
                            AutoConvertHelper.checkForDeals(dupeIds);
                        
                        //If Open Deal(s), send email with link to Deal 
                        //Owner of the most recently created Deal
                        if(existingDeals.size() > 0){
                            AutoConvertHelper.sendEmailToDealOwner(
                                existingDeals[0].Id, 
                                existingDeals[0].OwnerId);
                        }
                        else{//If no existing deals
                            
                            //Check for Call records:
                            List<Call__c> existingCalls = 
                                AutoConvertHelper.checkForCalls(dupeIds);
                            
                            
                            //If Call records exist:
                            If(existingCalls.size() > 0){
                                Call__c lastCall = existingCalls[0];
                                
                                //Check if Prospect flagged as Do Not Call
                                Boolean dnc =
                                    AutoConvertHelper.checkDNC(
                                        lastCall.Prospect__c);
                                
                                if(dnc = true){
                                    //[TODO]------------->                //flip DNC  
                                    AutoConvertHelper.flipDNC(
                                        lastCall.Prospect__c);
                                }
                                
                                Decimal nextAttempt = 
                                    lastCall.Attempt__c + 1.0;
                                Id pattern = lastCall.Call_Pattern__c;
                                
                                //If last call is Open 
                                if(lastCall.Status__c != 'Completed'){
                                    lastCall.Earliest_Call_Time__c = 
                                        System.now();
                                    lastCall.Scheduled__c = true;                           
                                    update lastCall;
                                }
                                else{//If last Call is Completed 
                                    
                                    //Create new Call record with
                                    //attempt and call template = Attempt +1
                                    AutoConvertHelper.createCall(
                                        lastCall.Prospect__c, pattern, 
                                        nextAttempt);
                                }	
                                
                                
                            }// End IF branch of If(existingCalls.size() > 0)
                            
                            else{ //Else if no Call records exist:	
                                
                                //Create Call record with currently active call 
                                //pattern, attempt = 1
                                Call_Pattern__c[] patterns = 
                                    [SELECT Id
                                     FROM Call_Pattern__c 
                                     WHERE Active__c = true 
                                     ORDER BY CreatedDate desc LIMIT 1];
                                
                                Call_Pattern__c pattern = patterns[0];
                                //If no Deals and no Calls, use first 
                                //Prospect Id
                                Id pId = dupeIds[0];
                                AutoConvertHelper.createCall(pId, 
                                                             pattern.Id, 1.0);
                                
                            }// End ELSE branch of if(existingCalls.size() > 0)
                            
                        } //End ELSE branch of if(existingDeals.size() > 0)
                        
                    } // End IF branch of if(dupeProspects.size() > 0)
                    
                    else{ //Else if duplicate Prospect record not found:                
                        
                        //Create Prospect__c record
                        Prospect__c p = new Prospect__c(); 
                        p.Name = lead.FirstName + ' ' + lead.LastName;
                        p.First_Name__c = lead.FirstName; 
                        p.Middle_Name__c = lead.Middle_Name__c; 			
                        p.Last_Name__c = lead.LastName; 
                        p.Personal_Email__c = lead.Email;
                        p.Mobile_SMS_Phone__c = lead.ext_phone_3__c; 
                        p.Home_Phone__c = lead.Phone;
                        p.Work_Phone__c = lead.ext_phone_2__c; 
                        p.Street__c = lead.Street; 
                        p.Street_2__c = lead.ext_address_2__c;
                        p.City__c = lead.City; 
                        p.State__c = lead.State; 
                        p.Postal_Code__c = lead.PostalCode;
                        p.County__c = lead.County__c;
                        p.Lead__c = lead.Id;
                        p.IP_Address__c = lead.IP_Address__c;
                        p.Work_Phone__c = lead.Work_Phone__c;
                        p.Total_Unsecured_Debt__c = 
                            lead.Total_Unsecured_Debt__c;
                        p.Asset_List__c = lead.Asset_List__c;
                        p.Income_Types__c = lead.Income_Types__c;
                        p.Filing_Reason__c = lead.Filing_Reason__c;
                        p.Garnishment__c = lead.Garnishment__c;
                        p.Creditor_Harrassment__c = 
                            lead.Creditor_Harrassment__c;
                        p.Divorce__c = lead.Divorce__c;
                        p.Lawsuits__c = lead.Lawsuits__c;
                        p.Illness_Disability__c = lead.Illness_Disability__c;
                        p.Repossession__c = lead.Repossession__c;
                        p.Loss_of_Income__c = lead.Loss_of_Income__c;
                        p.Foreclosure__c = lead.Foreclosure__c;
                        p.Other_Hardship__c = lead.Other_Hardship__c;
                        p.Bills__c = lead.Bills__c;
                        p.Expenses__c = lead.Expenses__c;
                        p.Gross_Monthly_Income__c = 
                            lead.Gross_Monthly_Income__c;
                        p.External_Lead_ID__c = lead.LeadId__c;
                        p.Lead_Campaign__c = lead.Lead_Campaign__c;
                        p.Lead_Source__c = lead.LeadSource;
                        p.Lead_Campaign_Sub_ID__c = 
                            lead.Lead_Campaign_Sub_ID__c;
                        
                        prosps_to_create_with_calls.add(p); 
                        
                    }    // End ELSE branch of if(dupeProspects.size() > 0)
                    
                } // End ELSE branch of if(existingCaseFiles.size() > 0)
                
            } // <------  END BOBERDOO LOGIC ------>
            
            
            // <------  BEGIN PHONE LEAD LOGIC ------>
            else if (lead.LeadSource == 'Phone_Lead') {
                Prospect__c p = new Prospect__c(); 
                p.Name = 'Phone Lead';
                p.First_Name__c = 'Phone'; 
                p.Last_Name__c = 'Lead';
                p.Mobile_SMS_Phone__c = lead.ext_phone_3__c; 
                p.Home_Phone__c = lead.Phone;
                p.Work_Phone__c = lead.ext_phone_2__c; 
                p.Street__c = lead.Street; 
                p.Street_2__c = lead.ext_address_2__c;
                p.City__c = lead.City; 
                p.State__c = lead.State; 
                p.Postal_Code__c = lead.PostalCode;
                p.County__c = lead.County__c;
                p.Lead__c = lead.Id;
                p.Lead_Source__c = lead.LeadSource;
                p.Lead_Campaign__c = lead.Lead_Campaign__c;
                p.Personal_Email__c = lead.Email;
                
                
                prosps_to_create.add(p); 
            }
            // <------  END PHONE LEAD LOGIC ------>
            
            // <------  BEGIN HOT TRANSFER LOGIC ------>    
            
            else if (lead.LeadSource == 'Hot Transfer') {
                
                Prospect__c p = new Prospect__c(); 
                p.Name = lead.FirstName + ' ' + lead.LastName;
                p.First_Name__c = lead.FirstName; 
                p.Middle_Name__c = lead.Middle_Name__c; 			
                p.Last_Name__c = lead.LastName; 
                p.Personal_Email__c = lead.Email;
                p.Mobile_SMS_Phone__c = lead.ext_phone_3__c; 
                p.Home_Phone__c = lead.Phone;
                p.Work_Phone__c = lead.ext_phone_2__c; 
                p.Street__c = lead.Street; 
                p.Street_2__c = lead.ext_address_2__c;
                p.City__c = lead.City; 
                p.State__c = lead.State; 
                p.Postal_Code__c = lead.PostalCode;
                p.County__c = lead.County__c;
                p.Lead__c = lead.Id;
                p.IP_Address__c = lead.IP_Address__c;
                p.Total_Unsecured_Debt__c = lead.Total_Unsecured_Debt__c;
                p.Asset_List__c = lead.Asset_List__c;
                p.Income_Types__c = lead.Income_Types__c;
                p.Filing_Reason__c = lead.Filing_Reason__c;
                p.Garnishment__c = lead.Garnishment__c;
                p.Creditor_Harrassment__c = lead.Creditor_Harrassment__c;
                p.Divorce__c = lead.Divorce__c;
                p.Lawsuits__c = lead.Lawsuits__c;
                p.Illness_Disability__c = lead.Illness_Disability__c;
                p.Repossession__c = lead.Repossession__c;
                p.Loss_of_Income__c = lead.Loss_of_Income__c;
                p.Foreclosure__c = lead.Foreclosure__c;
                p.Other_Hardship__c = lead.Other_Hardship__c;
                p.Bills__c = lead.Bills__c;
                p.Expenses__c = lead.Expenses__c;
                p.Gross_Monthly_Income__c = 
                    lead.Gross_Monthly_Income__c;
                p.External_Lead_ID__c = lead.LeadId__c;
                p.Lead_Campaign__c = lead.Lead_Campaign__c;
                p.Lead_Source__c = lead.LeadSource;
                p.Lead_Campaign_Sub_ID__c = 
                    lead.Lead_Campaign_Sub_ID__c;
                
                prosps_to_create.add(p);
                
            }
            // <------  END HOT TRANSFER LOGIC ------>  
            
        }
        
        
        if (prosps_to_create.size() > 0){
            insert prosps_to_create;
        }
        
        
        if (prosps_to_create_with_deals.size() > 0){
            insert prosps_to_create_with_deals;
            
            for(Prospect__c p: prosps_to_create_with_deals){
                Map<String, String> deal_fields = 
                    lead_fields_for_deal.get(p.Lead__c);
                
                Deal__c d = new Deal__c();
                d.Name = p.Name;
                d.Prospect__c = p.Id; 
                d.Motivator__c = deal_fields.get('motivator'); 
                d.Pain_Point__c = deal_fields.get('pain_point');
                d.Hope__c = deal_fields.get('hope');
                d.Facts_about_Debt__c = deal_fields.get('facts');
                d.Lead_Campaign__c = deal_fields.get('lead_campaign');
                d.Velocify_ID__c = deal_fields.get('velocifyID');
                
                deals_to_create.add(d);
            }
            
            insert deals_to_create;
        }
        
        
        if (prosps_to_create_with_calls.size() > 0){
            insert prosps_to_create_with_calls;
            
            //Query for currently active Call_Pattern__c
            Call_Pattern__c pattern = [SELECT Id, Prospects__c 
                                       FROM Call_Pattern__c 
                                       WHERE Active__c = true 
                                       ORDER BY CreatedDate desc LIMIT 1];
            Call_Template__c template = [SELECT Id, Team__c 
                                         FROM Call_Template__c 
                                         WHERE Call_Pattern__c =: pattern.Id 
                                         AND Attempt__c = 1];
            
            List<RecordType> RT_IDs = [SELECT Id FROM RecordType WHERE Name = 'Prospect Call' AND sObjectType = 'Call__c'];
	        Id RT_ID = RT_IDs[0].Id;
            
            for(Prospect__c p : prosps_to_create_with_calls){
                //Create Call__c record
                Call__c call = new Call__c();
                call.RecordTypeId = RT_ID;
                call.Prospect__c = p.Id;
                call.Attempt__c = 1;
                call.Call_Template__c = template.Id;
                call.Earliest_Call_Time__c = System.now();
                call.Team__c = template.Team__c;
                call.Status__c = 'Not Started';
                call.Call_Pattern__c = pattern.Id;
                
                p.Call_Pattern__c = pattern.Id;
                
                calls_to_create.add(call); 
                prosps_to_update.add(p);      
                
            }
            
            Double prospectCount;
            
            if (pattern.Prospects__c == null){
                prospectCount = calls_to_create.size();    
            }
            else{
                prospectCount = 
                    pattern.Prospects__c + calls_to_create.size();
            }
            
            
            AutoConvertHelper.incrementCallPatternProspects(pattern.Id, 
                                                            prospectCount);
            update prosps_to_update;
            insert calls_to_create;
        }
    }
}