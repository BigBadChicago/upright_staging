trigger Populate_Time_Zone on Prospect__c (before insert, before update) {
    
    if(Trigger.isInsert){
        //Populate Zip Code Lookup
        Set<String> allZipStrings = new Set<String>();
        
        //loop through trigger.new to grab all zipcodes in the trigger set
        for (Prospect__c p : Trigger.new){
            

            if(p.Postal_Code__c != null){
                allZipStrings.add(p.Postal_Code__c.substring(0,5));
            }
            
        }
   
        //query Zip_Code__c for records that match the trigger set (grab 90210 in the event of an invalid ZIP)
        List <Zip_Code__c> relatedZips = [SELECT Id, Time_Zone__c, City__c, State__c, Name FROM Zip_Code__c WHERE Name in : allZipStrings OR Name = '90210'];

        //build a map of zipcode string to zipcode Id to use later to populate the field
        Map<String, Zip_Code__c> zipMap = new Map<String, Zip_Code__c>();
        if(!relatedZips.isEmpty()){
            for (Zip_Code__c zc : relatedZips){
                zipMap.put(zc.Name, zc);
            }
        }
        
        //loop through trigger set and populate related zip records based on Postal_Code__c string
        for (Prospect__c p : trigger.new){
            if(p.Postal_Code__c!= null && zipMap.get(p.Postal_Code__c.substring(0, 5)) != null){
                p.Zip_Code_Lookup__c = zipMap.get(p.Postal_Code__c.substring(0, 5)).Id;
                p.Time_Zone_Lookup__c = zipMap.get(p.Postal_Code__c.substring(0, 5)).Time_Zone__c;
                p.City__c = zipMap.get(p.Postal_Code__c.substring(0, 5)).City__c;
                p.State__c = zipMap.get(p.Postal_Code__c.substring(0, 5)).State__c;
                System.debug('Successfully updated Prospect Info >> ' + p);
            }
            else{
                System.debug('Hit error branch, zip wasn\'t found in zipMap >> ' + p);
                p.Zip_Code_Lookup__c = zipMap.get('90210').Id;
                p.Time_Zone_Lookup__c = zipMap.get('90210').Time_Zone__c;
                
            }
        }
        
    }
     
    
    if(Trigger.isUpdate){
        
        //If Postal_Code__c has changed, update Zip_Code_Lookup__c and Time_Zone_Lookup__c 
        //If Zip_Code_Lookup__c has changed, update the Postal_Code__c to keep them in sync
        
        //Collection of postal_code strings and Prospects for those needing updates
        Set<String> allZipStrings = new Set<String>();
        List<Prospect__c> postal_code_changes = new List<Prospect__c>();
        
        //Collection of zip_code_lookup Ids and Prospects for those needing updates
        Set<String> allZipIds = new Set<String>();
        List<Prospect__c> zip_code_lookup_changes = new List<Prospect__c>();
        
        //Loop through trigger set to check if Postal_Code__c or Zip_Code_Lookup__c has changed
        for (Prospect__c p : Trigger.new) {                        
            Prospect__c old_p = Trigger.oldMap.get(p.Id);
            
            //If postal code has changed and is not null add to list of prospect records to update
            if(p.Postal_Code__c != null && p.Postal_Code__c != old_p.Postal_Code__c){
                allZipStrings.add(p.Postal_Code__c.substring(0,5)); 
                postal_code_changes.add(p);
            }
            
            //if zip_code_lookup has changed and is not null add to list add to list of prospect records to update
            if(p.Zip_Code_Lookup__c != null && p.Zip_Code_Lookup__c != old_p.Zip_Code_Lookup__c){
                allZipIds.add(p.Zip_Code_Lookup__c); 
                zip_code_lookup_changes.add(p);
            }            
        }
        
        //If any postal_codes have changed, look up the related Zip_Code__c records and add to 
        //map that will be used to update the Zip_Code_Lookup__c and Time_Zone_Lookup__c for each record
        if (!postal_code_changes.isEmpty()){
            
            List <Zip_Code__c> relatedZips = [SELECT Id, Time_Zone__c, City__c, State__c, Name FROM Zip_Code__c WHERE Name in : allZipStrings OR Name = '90210'];             
            Map<String, Zip_Code__c> zipMap = new Map<String, Zip_Code__c>();
            
            for (Zip_Code__c zc : relatedZips){
                zipMap.put(zc.Name, zc);
            }
            
            for (Prospect__c p : postal_code_changes){
                if(p.Postal_Code__c!= null && zipMap.get(p.Postal_Code__c.substring(0, 5)) != null){
                    p.Zip_Code_Lookup__c = zipMap.get(p.Postal_Code__c.substring(0, 5)).Id;
                    p.Time_Zone_Lookup__c = zipMap.get(p.Postal_Code__c.substring(0, 5)).Time_Zone__c;
                    p.City__c = zipMap.get(p.Postal_Code__c.substring(0, 5)).City__c;
                    p.State__c = zipMap.get(p.Postal_Code__c.substring(0, 5)).State__c;
                }
                else{
                    p.Zip_Code_Lookup__c = zipMap.get('90210').Id;
                    p.Time_Zone_Lookup__c = zipMap.get('90210').Time_Zone__c;
                }
            }
        }
        
        if(!zip_code_lookup_changes.isEmpty()){
            List<Zip_Code__c> updateZips = [SELECT Name, Time_Zone__c FROM Zip_Code__c WHERE Id in : allZipIds];            
            Map<Id, Zip_Code__c> postalMap = new Map<Id, Zip_Code__c>();
            
            for (Zip_Code__c zc : updateZips){
                postalMap.put(zc.Id, zc);
            }
            
            for (Prospect__c p : zip_code_lookup_changes){
                p.Postal_Code__c = postalMap.get(p.Zip_Code_Lookup__c).Name.substring(0, 5);
                p.Time_Zone_Lookup__c = postalMap.get(p.Zip_Code_Lookup__c).Time_Zone__c;
            }
        }        
    }
    
}