trigger Deal_Marked_as_Hired on Deal__c (after insert, after update) {
    List<Call_Pattern__c> cps_to_update = new List<Call_Pattern__c>();
    List<Prospect__c> prospects_to_update = new List<Prospect__c>();
    List<Call__c> calls_to_update = new List<Call__c>();

    List<Deal__c> deals = [SELECT Id, Prospect__c, Call__c FROM Deal__c WHERE Id IN: Trigger.new];
	Map<Id, Id> dealProspectMap = new Map<Id, Id>();    
	Map<Id, Id> dealCallMap = new Map<Id, Id>();
    for (Deal__c d : deals){
        dealProspectMap.put(d.Id, d.Prospect__c);
        dealCallMap.put(d.Id, d.Call__c);
    }
  	
    List<Prospect__c> prospectList = [SELECT Id, Call_Pattern__c, Hired__c FROM Prospect__c WHERE Id IN: dealProspectMap.values()];
	Map<Id, Prospect__c> prospects = new Map<Id, Prospect__c>();
    Map<Id, Id> prospectCallPatternMap = new Map<Id, Id>();
    for(Prospect__c p : prospectList){
        prospects.put(p.Id, p);
        prospectCallPatternMap.put(p.Id, p.Call_Pattern__c);
    }
    
    
    List<Call_Pattern__c> callPatternList = [SELECT Id, Hires__c FROM Call_Pattern__c WHERE Id IN: prospectCallPatternMap.values()];
    Map<Id, Call_Pattern__c> callPatterns = new Map<Id, Call_Pattern__c>();
    for (Call_Pattern__c cp : callPatternList){
        callPatterns.put(cp.Id, cp);
    }
    
    List<Call__c> callList = [SELECT Id, Hired__c FROM Call__c WHERE Id IN: dealCallMap.values()];
    Map<Id, Call__c> calls = new Map<Id, Call__c>();
    for (Call__c c : callList){
        calls.put(c.Id, c);
    }
    
    
    
    if(Trigger.isInsert){

        for (Deal__c d : Trigger.new){
            if(d.Stage__c == 'Hired'){
                Prospect__c p = prospects.get(d.Prospect__c);
                
                //populate 'Hired' timestamp on Prospect record
                p.Hired__c = System.now();
                prospects_to_update.add(p);
                
                //populate 'Hired' timestamp on Call record
                if (d.Call__c != null){
                    Call__c c = calls.get(d.Call__c);
                    c.Hired__c = System.now();
                    calls_to_update.add(c);
                }
                
                //increment 'Hires' aggregate count on Call Pattern
                if (p.Call_Pattern__c != null){  
                    Call_Pattern__c cp = callPatterns.get(p.Call_Pattern__c);
                    cp.Hires__c = cp.Hires__c == null ? 1 : cp.Hires__c + 1;                        
    				cps_to_update.add(cp);                
                }                                
            }
        }
        if(prospects_to_update.size() > 0){
            update prospects_to_update;
        }
        
        if(calls_to_update.size() > 0){
            update calls_to_update;
        }
        
        if(cps_to_update.size() > 0){
            update cps_to_update;
        }
    }
    
    if(Trigger.isUpdate){
        for (Deal__c d : Trigger.new){
            Deal__c old_d = Trigger.oldMap.get(d.Id);
            if(d.Stage__c == 'Hired' && old_d.Stage__c != 'Hired'){
                
                Prospect__c p = prospects.get(d.Prospect__c);
                
                //populate 'Hired' timestamp on Prospect record
                p.Hired__c = System.now();
                prospects_to_update.add(p);
                
                 //populate 'Hired' timestamp on Call record
                if (d.Call__c != null){
                    Call__c c = calls.get(d.Call__c);
                    c.Hired__c = System.now();
                    calls_to_update.add(c);
                }
                
                //increment 'Hires' aggregate count on Call Pattern
                if (p.Call_Pattern__c != null){  
                    Call_Pattern__c cp = callPatterns.get(p.Call_Pattern__c);
                    cp.Hires__c = cp.Hires__c == null ? 1:cp.Hires__c + 1;   
                    cps_to_update.add(cp);
                    
                }
            }
            
        }
        if(prospects_to_update.size() > 0){
            update prospects_to_update;
        }
        
        if(calls_to_update.size() > 0){
            update calls_to_update;
        }
        
        if(cps_to_update.size() > 0){
            update cps_to_update;
        }
    }
    
}