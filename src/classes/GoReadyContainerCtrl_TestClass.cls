@isTest
public class GoReadyContainerCtrl_TestClass {
    
    //Ctrl Methods:
    //fetchRawPANumbersList
    //startApplicationSessionClock
    //stopApplicationSessionClock
    //increaseCallTime
    //fetchCall
    //fetchCount
    //fetchCaseFile
    //fetchContact
    //updateAppSessionWithActivity
    //updateAppSessionWithMissedConnection
    
    
    //Test Objects Needed:
    //pa2: PA User with Name, Phone, MobilePhone and 
    //pa1: John Fox
    //c: Contact: 
    //c2: Contact with DNC = True
    //cf: Name, PA2 as Partner Attorney
    //pi_call: Name, Earliest_Call_Time, cf as Case File, RecordTypeId WHERE Name = 'Partner Intro Call'
    //h_call: Name, Earliest_Call_Time, cf as Case File, RecordTypeId WHERE Name = 'Handoff Call'
    //paod_call: Name, Earliest_Call_Time, cf as Case File, RecordTypeId WHERE Name = 'PA On-Demand Call'
    
    
    //Tests: 
    //Run as pa2(fetchRawPANumbersList)
    //Run as pa1(fetchRawPANumbersList)
    //Run as pa2(startAppSessionClock), assert return value is valid 18 digitID
    //Run as pa2(stopAppSessionClock), query app session and confirm AppSessionEndTime__c and Session_Time__c not null
    //increaseCallTime - query for Call Id, run increaseCallTime and confirm that EarliestCallTime > System.now()
    //fetchCall
    
    @testSetup static void GR_TestData() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Attorney']; 
        
        //Create Partner Attorney User with 2 phone numbers
        User pa2 = new User(Alias = 'pa2', Email='pa2@testorg.com', 
                            EmailEncodingKey='UTF-8', FirstName= 'PA', LastName='2', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = p.Id, Phone='312-555-1212', MobilePhone='312-555-1234',
                            TimeZoneSidKey='America/Chicago', UserName='pa2@testorg.com');
        insert pa2;
        
        //Create Partner Attorney User with 1 phone number
        User pa1 = new User(Alias = 'pa1', Email='pa2@testorg.com', 
                            EmailEncodingKey='UTF-8', FirstName= 'PA', LastName='1', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = p.Id, Phone='312-555-4567',
                            TimeZoneSidKey='America/Chicago', UserName='pa1@testorg.com');
        insert pa1;
        
        
        //Create Test Contact
        Contact c = new Contact();
        c.LastName = 'TEST';
        c.DNC_Auto_Dial__c = False;
        insert c;
        
        //Create Test Contact w/ DNC = True
        Contact dnc_c = new Contact();
        dnc_c.LastName = 'TEST';
        dnc_c.DNC_Auto_Dial__c = True;
        insert dnc_c;        
        
        //Create Test Case_File__c
        Case_File__c cf = new Case_File__c();
        cf.Partner_Attorney_User__c = pa2.Id;
        cf.Stage__c = 'Waiting on Payments';
        cf.Primary_Contact__c = c.Id;
        insert cf;
        
        //Create Test Case_File__c
        Case_File__c dnc_cf = new Case_File__c();
        dnc_cf.Partner_Attorney_User__c = pa2.Id;
        dnc_cf.Stage__c = 'Waiting on Payments';
        dnc_cf.Primary_Contact__c = dnc_c.Id;
        insert dnc_cf;
        
        //Create Test Calls
        List<RecordType> rts = [SELECT Id, Name FROM RecordType WHERE sObjectType = 'Call__c' AND Name != 'Prospect'];
        
        Map<String, Id> rt_ids = new Map<String, Id>();
        for (integer i = 0; i < rts.size(); i++){
            rt_ids.put(rts[i].Name, rts[i].Id);
        }            
        
        Call__c pi_call = new Call__c();
        pi_call.Case_File__c = cf.Id;
        pi_call.Status__c = 'Not Started';
        pi_call.Earliest_Call_Time__c = System.now();
        pi_call.RecordTypeId = rt_ids.get('Partner Intro Call');
        insert pi_call;
        
        Call__c h_call = new Call__c();
        h_call.Case_File__c = cf.Id;
        h_call.Status__c = 'Not Started';
        h_call.Earliest_Call_Time__c = System.now();
        h_call.RecordTypeId = rt_ids.get('Handoff Call');
        insert h_call;
        
        Call__c paod_call = new Call__c();
        paod_call.Case_File__c = cf.Id;
        paod_call.Status__c = 'Not Started';
        paod_call.Earliest_Call_Time__c = System.now();
        paod_call.RecordTypeId = rt_ids.get('PA On-Demand Call');  
        insert paod_call;
        
        //insert a call where Case_File__r.DNC_Autodial__c == True to confrim it's not returned
        Call__c dnc_pi_call = new Call__c();
        dnc_pi_call.Case_File__c = dnc_cf.Id;
        dnc_pi_call.Status__c = 'Not Started';
        dnc_pi_call.Earliest_Call_Time__c = System.now();
        dnc_pi_call.RecordTypeId = rt_ids.get('Partner Intro Call');
        insert dnc_pi_call;
    }
    
    
    public static TestMethod void testfetch2PANums(){
        User pa2 = [SELECT Id FROM User WHERE UserName = 'pa2@testorg.com'];
        
        Test.startTest();
        System.runAs(pa2){
            GoReadyContainerCtrl.fetchRawPANumbersList();
        }  
        Test.stopTest();
    }
    
    public static TestMethod void testfetch1PANum(){
        User pa1 = [SELECT Id FROM User WHERE UserName = 'pa1@testorg.com'];
       
        Test.startTest();
        System.runAs(pa1){
            GoReadyContainerCtrl.fetchRawPANumbersList();
        }
        Test.stopTest();
    }   
    
    public static TestMethod void testStartAppSessionClock(){
        User pa2 = [SELECT Id FROM User WHERE UserName = 'pa2@testorg.com'];
        
        Test.startTest();
        System.runAs(pa2){
            Id appSessId = GoReadyContainerCtrl.startApplicationSessionClock();
            System.assert(appSessId != null);
            GoReadyContainerCtrl.stopApplicationSessionClock();
            App_Session__c app_sess = [SELECT Id, AppSessionEndTime__c, OwnerId, Session_Time__c FROM App_Session__c WHERE Id =: appSessId];
            System.assert(app_sess.AppSessionEndTime__c != null && app_sess.Session_Time__c != null && app_sess.OwnerId == pa2.Id);
        }  
        Test.stopTest();
    }
    
    public static TestMethod void testIncreaseCallTime() {
        Call__c call = [SELECT Id, Earliest_Call_Time__c FROM Call__c LIMIT 1];
        
        Test.startTest();
        GoReadyContainerCtrl.increaseCallTime(call.Id);
        Call__c updated_call = [SELECT Id, Earliest_Call_Time__c FROM Call__c WHERE Id =: call.Id];
        System.assert(updated_call.Earliest_Call_Time__c > System.now());
        Test.stopTest();
    
    }
    
    
    public static TestMethod void testFetchCall(){
        List<String> call_types = new List<String>{'Partner Intro Call', 'Handoff Call', 'PA On-Demand Call'};
        User pa2 = [SELECT Id FROM User WHERE UserName = 'pa2@testorg.com'];
        
        Test.startTest();
        System.runAs(pa2){
            GoReadyContainerCtrl.fetchCall(call_types);
        }
        Test.stopTest();
    }
    
    public static TestMethod void testFetchCount(){
        List<String> call_types = new List<String>{'Partner Intro Call', 'Handoff Call', 'PA On-Demnad Call'};
        User pa2 = [SELECT Id FROM User WHERE UserName = 'pa2@testorg.com'];
        
        Test.startTest();
        System.runAs(pa2){
            GoReadyContainerCtrl.fetchCount(call_types);
        }
        Test.stopTest();
    }
    
    public static TestMethod void testFetchCaseFile(){
        Case_File__c cf = [SELECT Id FROM Case_File__c LIMIT 1];
        
        Test.startTest();
        Case_File__c returned = GoReadyContainerCtrl.fetchCaseFile(cf.Id);
        System.assert(returned.Id != null);
        Test.stopTest();
    }
    
    public static TestMethod void testFetchContact(){
        Contact c = [SELECT Id FROM Contact LIMIT 1];
        Test.startTest();
        Contact returned = GoReadyContainerCtrl.fetchContact(c.Id);
        System.assert(returned.Id != null);
        Test.stopTest();  
    }
    
    public static TestMethod void testupdateAppSessionWithActivity(){
        User pa2 = [SELECT Id FROM User WHERE UserName = 'pa2@testorg.com'];
        Case_File__c cf = [SELECT Id, Primary_Contact__c FROM Case_File__c WHERE Partner_Attorney_User__c =: pa2.Id LIMIT 1];
        Call__c call = [SELECT Id FROM Call__c WHERE Case_File__c =: cf.Id LIMIT 1];        

        Test.startTest();
        System.runAs(pa2){
            Id appSessId = GoReadyContainerCtrl.startApplicationSessionClock();
            GoReadyContainerCtrl.updateAppSessionWithActivity(appSessId, cf.Id, call.Id, cf.Primary_Contact__c);
        }
        Test.stopTest();        
    }

    public static TestMethod void testupdateAppSessionWithMissedConnection(){
        User pa2 = [SELECT Id FROM User WHERE UserName = 'pa2@testorg.com'];
        Case_File__c cf = [SELECT Id, Primary_Contact__c FROM Case_File__c WHERE Partner_Attorney_User__c =: pa2.Id LIMIT 1];
        Call__c call = [SELECT Id FROM Call__c WHERE Case_File__c =: cf.Id LIMIT 1];        

        Test.startTest();
        System.runAs(pa2){
            Id appSessId = GoReadyContainerCtrl.startApplicationSessionClock();
            GoReadyContainerCtrl.updateAppSessionWithMissedConnection(appSessId, cf.Id, call.Id, cf.Primary_Contact__c);
        }
        Test.stopTest();        
    }
    
}