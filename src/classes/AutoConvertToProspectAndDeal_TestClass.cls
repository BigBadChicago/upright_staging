@isTest
public class AutoConvertToProspectAndDeal_TestClass {
    private static Integer LEAD_COUNT = 0;
    
    private static Lead createLead() {
        Time_Zone__c tz = New Time_Zone__c(Name='America/Chicago', Standard_Offset__c=-6, DST__c=true);
        insert tz;
        
        Zip_Code__c zip = New Zip_Code__c(Name='60630', Time_Zone__c=tz.Id);
        insert zip;
        
        Time_Zone__c test_tz = new Time_Zone__c(Name='America/Los_Angeles', Standard_Offset__c = -8, DST__c = true);
        insert test_tz;
        Zip_Code__c test_z = new Zip_Code__c(Name='90210', Time_Zone__c=test_tz.Id);
        insert test_z;
        
        Call_Pattern__c cp = new Call_Pattern__c();
        cp.Active__c = true;
        cp.Name = 'Test_Call_Pattern';
        cp.Unlimited_Calls__c = true;
        insert cp;
        
        Call_Template__c ct = new Call_Template__c(); 
        ct.Call_Pattern__c = cp.Id;
        ct.Attempt__c = 1;
        ct.Min_Hours_Since_Previous_Call__c = 0;
        ct.Team__c = 'A';
        insert ct;

        
        LEAD_COUNT += 1;
        return new Lead(
            FirstName = '_unittest_firstname_: ' + LEAD_COUNT,
            LastName = '_unittest_firstname_: ' + LEAD_COUNT,
            Company = '_unittest_company_: ',
            Status = 'Open',
            PostalCode = '60603'
            
        );
    }
    
    public static void setLeadSource(Lead lead, String source) {
        
        lead.LeadSource = source;
        
    }
    
    private static List<Prospect__c> fetchProspects(Set<Id> ids) {
        return [
            select Id, Name
            from Prospect__c
            where Lead__c in :ids
        ];
    }
    
    private static List<Deal__c> fetchDeals(Set<Id> ids) {
        return [
            select Id, Name, Prospect__c
            from Deal__c
            where Prospect__c in :ids
        ];
    }
    
    private static List<Call__c> fetchCalls(Set<Id> ids) {
        return [
            select Id, Name, Prospect__c
            from Call__c
            where Prospect__c in :ids
        ];
    }
    
    public static testMethod void singleLeadFromVelocify() {
        Lead testLead = createLead();
        setLeadSource(testLead, 'Velocify_Prospect');
        System.debug(testLead.LeadSource);
        Test.startTest();
        insert testLead;
        Test.stopTest();
        
        List<Prospect__c> fetchedProspects = fetchProspects(new Set<Id> {testLead.Id});
        System.assertEquals(1, fetchedProspects.size(), 'Didn\'t get a Prospect returned');
        System.assertEquals(fetchedProspects.get(0).Name, testLead.FirstName + ' ' + testLead.LastName, 'Lead and Prospect Names don\'t match'); 
        
        List<Deal__c> fetchedDeals = fetchDeals(new Set<Id> {fetchedProspects.get(0).Id});
        System.assertEquals(1, fetchedDeals.size(), 'Didn\'t get a Deal returned');		
        System.assertEquals(fetchedDeals.get(0).Name, fetchedProspects.get(0).Name, 'Prospect and Deal names don\'t match');
    }
    
    public static testMethod void singleLeadFromHotTransfer() {
        Lead testLead = createLead();
        setLeadSource(testLead, 'Hot Transfer');
        System.debug(testLead.LeadSource);
        Test.startTest();
        insert testLead;
        Test.stopTest();
        
        List<Prospect__c> fetchedProspects = fetchProspects(new Set<Id> {testLead.Id});
        System.assertEquals(1, fetchedProspects.size(), 'Didn\'t get a Prospect returned');
        System.assertEquals(fetchedProspects.get(0).Name, testLead.FirstName + ' ' + testLead.LastName, 'Lead and Prospect Names don\'t match'); 
        
        List<Deal__c> fetchedDeals = fetchDeals(new Set<Id> {fetchedProspects.get(0).Id});
        System.assertEquals(0, fetchedDeals.size(), 'Deal was returned');		
        
    }
    
    public static testMethod void singleLeadFromBoberdoo() {
        Lead testLead = createLead();
        setLeadSource(testLead, 'boberdoo');
        System.debug(testLead.LeadSource);
        Test.startTest();
        insert testLead;
        Test.stopTest();
        
        List<Prospect__c> fetchedProspects = fetchProspects(new Set<Id> {testLead.Id});
        System.assertEquals(1, fetchedProspects.size(), 'Didn\'t get a Prospect returned');
        System.assertEquals(fetchedProspects.get(0).Name, testLead.FirstName + ' ' + testLead.LastName, 'Lead and Prospect Names don\'t match'); 
        
        List<Call__c> fetchedCalls = fetchCalls(new Set<Id> {fetchedProspects.get(0).Id});
        System.assertEquals(1, fetchedCalls.size(), 'Didn\'t get a Call returned');		
        
        
    }
    
    public static testMethod void singlePhoneLead() {
        Lead testLead = createLead();
        setLeadSource(testLead, 'phone_lead');
        System.debug(testLead.LeadSource);
        Test.startTest();
        insert testLead;
        Test.stopTest();
        
        List<Prospect__c> fetchedProspects = fetchProspects(new Set<Id> {testLead.Id});
        System.assertEquals(1, fetchedProspects.size(), 'Didn\'t get a Prospect returned');
        System.assertEquals(fetchedProspects.get(0).Name, 'Phone Lead', 'Name isn\'t \'Phone Lead\''); 
        
    }
    
}