public with sharing class ProspectSearchCriteriaCtrl {
    
    /*
     * Loads the initial value of the given SObject type with ID "value"
	 */
    
	@AuraEnabled
	public static List<sObject> getSuggestions(String searchFieldType, String term, Integer limitSize) {
        // could add in logic to remove possible duplicate fields
    	String soql = 
            ' SELECT Name, Id, First_Name__c, Last_Name__c, Home_Phone__c, Work_Phone__c, Mobile_SMS_Phone__c, Personal_Email__c, Postal_Code__c, Time_Zone__c, DNC__c, Time_Zone_Lookup__c, State__c, Why__c, Hope__c, Facts_about_Debt__c FROM Prospect__c';
        if(searchFieldType == 'Name'){
            soql += ' WHERE Name Like \'%' + String.escapeSingleQuotes(term) + '%\'';
        } 
        if(searchFieldType == 'Phone'){
            soql += ' WHERE Home_Phone__c Like \'%' + String.escapeSingleQuotes(term) + '%\' OR Mobile_SMS_Phone__c Like \'%' + String.escapeSingleQuotes(term) + '%\' OR Work_Phone__c Like \'%' + String.escapeSingleQuotes(term) + '%\'';
        } 
        soql += ' LIMIT ' + limitSize;
        return Database.query(soql);
    }

	@AuraEnabled
	public static Call__c getProspectCalls(String prospectID) {
        System.debug('***JB*** ProspectID: ' + prospectID);
        List<RecordType> RT_IDs = [SELECT Id FROM RecordType WHERE Name = 'Prospect Call' AND sObjectType = 'Call__c'];
        Id RT_ID = RT_IDs[0].Id;
        List<Call__c> callList = new List<Call__c>(); 
        callList = [SELECT Name, Id, Attempt__c, Call_Pattern__c, Call_Pattern__r.Name, Inbound_Call__c, Locked__c, Locked_Time__c, Locked_By__c, Call_Template__c, Call_Template__r.Name, Prospect__c, Status__c, Team__c, Time_Zone__c, Scheduled__c, Lead_Score__c, Earliest_Call_Time__c FROM Call__c WHERE Prospect__c =: prospectID AND RecordType.Name = 'Prospect Call' ORDER BY Attempt__c desc];
        if(callList.size() == 0){
            Call__c firstCall = new Call__c();
            Call_Pattern__c callPat = [SELECT Id, Name, Active__c FROM Call_Pattern__c WHERE Active__c = true LIMIT 1];
        	Call_Template__c callTemp = [SELECT Id, Name, Attempt__c, Call_Pattern__c, Team__c, Email_Template__c, SMS_Template__c FROM Call_Template__c WHERE Attempt__c = 1 AND Call_Pattern__c =: callPat.Id LIMIT 1];
            firstCall.Prospect__c = prospectID;
            firstCall.Attempt__c = 1;
            firstCall.RecordTypeId = RT_ID;
            firstCall.Status__c = 'Not Started';
            firstCall.Inbound_Call__c = True;
			firstCall.Locked__c = true;
            firstCall.Locked_Time__c = Datetime.now(); 
            firstCall.Locked_By__c = userInfo.getUserId();
            firstCall.Call_Pattern__c = callPat.Id;
            firstCall.Call_Template__c = callTemp.Id;
            firstCall.Team__c = callTemp.Team__c;
            firstCall.Earliest_Call_Time__c = System.now();
            insert firstCall;
            return firstCall;    
        } else {
            Call__c latestCall =  callList[0];
            System.debug('***JB*** Latest Attempt: ' + callList[0].Attempt__c);
            if(latestCall.Locked__c == true){
                Call__c newCallUnlockedCall = new Call__c();
                newCallUnlockedCall.Prospect__c = latestCall.Prospect__c;
                newCallUnlockedCall.RecordTypeId = RT_ID;
                newCallUnlockedCall.Attempt__c = latestCall.Attempt__c + 1;
                newCallUnlockedCall.Status__c = 'Not Started';
                newCallUnlockedCall.Inbound_Call__c = True;
                newCallUnlockedCall.Locked__c = true;
                newCallUnlockedCall.Locked_Time__c = Datetime.now(); 
                newCallUnlockedCall.Locked_By__c = userInfo.getUserId();
                newCallUnlockedCall.Call_Pattern__c = latestCall.Call_Pattern__c;
                newCallUnlockedCall.Call_Template__c = latestCall.Call_Template__c;
                newCallUnlockedCall.Team__c = latestCall.Team__c;
                newCallUnlockedCall.Earliest_Call_Time__c = System.now();
                insert newCallUnlockedCall;
                return newCallUnlockedCall;
            } else {
                latestCall.Locked__c = true;
                latestCall.Locked_Time__c = Datetime.now();        	
                update latestCall;
                return latestCall;
            }
        }
    }    
      
}