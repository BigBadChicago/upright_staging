public class FirstResponseContainerCtrl {
    
    @AuraEnabled
    public static Call__c getFirstResponseCall(String validTimeZones) {
        
        System.debug('getfirstResponseCall >> firing');
        List<String> tz_list = 
            new List<String>(validTimeZones.replace('[', 
                                                    '').replace('"', '').replace(']', '').split('\\,'));
        for (String tz : tz_list){
            System.debug('getfirstResponseCall >> validTimeZone: ' + tz);
        }
        
        //Get userId and Team__c
        String userID = UserInfo.getUserId();
        User usr = [SELECT Id, Team__c FROM User WHERE Id =: userID];
        System.debug('getfirstResponseCall - usr >>' + usr);
        
        //If user team is null, default to 'C'
        String usr_team;
        if (usr.Team__c == null){
            usr_team = 'C';
        }
        else{
            usr_team = usr.Team__c;
        }
        
        Call__c last_15_min_call;
        Call__c overflow_call;
        
        if(usr.Team__c == 'A'){
            //Check for uncalled 1st Attempts from last 30 minutes 
            //Disregard time zone if Prospect__c was created in the 
            //past 30 minutes (i.e. Call #1 was created in past 30 mins)
            List<Call__c> last_15_min_calls = [SELECT Id, Attempt__c, Status__c, 
                                               Team__c, Locked__c, Earliest_Call_Time__c, 
                                               Call_Template__c, Call_Pattern__c, Scheduled__c, 
                                               Lead_Score__c, Prospect__c, Time_Zone__c
                                               FROM Call__c 
                                               WHERE Attempt__c = 1 
                                               AND Call__c.Locked__c != true 
                                               AND RecordType.Name = 'Prospect Call'
                                               AND Status__c != 'Completed' 
                                               AND CreatedDate >=: System.now().addMinutes(-15)
                                               ORDER BY CreatedDate asc LIMIT 1];
            
            //If list size > 0, populate Call__c to variable and lock it
            if(last_15_min_calls.size() > 0){
                last_15_min_call = last_15_min_calls[0];
                last_15_min_call.Locked__c = true;
                last_15_min_call.Locked_Time__c = Datetime.now(); 
                last_15_min_call.Locked_By__c = usr.Id;
                update last_15_min_call;
            }
            
        }
        
        //--OVERFLOW CHECK--//
        else{//else if Team != 'A'
            
            // If there are excessive uncalled first attempts (like the 
            //begining of the day or a massive campaign pushes tons of leads 
            //into our system), serve up 1st attempts to all reps regardless 
            // of team. 
            
            //A Team will get Last In First Out (newest first)
            //Other Teams will get First In First Out (oldest first)
            
            
            Integer overflowAmount = 50;
            
            
            // This query will pull the list of uncalled first attempts 
            //everytime the firstResponseConsole is loaded by anyone who 
            //is non Team A.
            List <Call__c> callList = [SELECT Id 
                                       FROM Call__c 
                                       WHERE Attempt__c =1 
                                       AND Status__c != 'Completed' 
                                       AND RecordType.Name = 'Prospect Call'
                                       AND Time_Zone__c IN: tz_list];
            Integer firstCalls = callList.size();
            System.debug('callList >> ' + callList);
            System.debug('firstCalls >> ' + firstCalls);
            
            // If uncalled 1st Attempts > overflowAmount have non Team A users 
            //attack overflow list from bottom up (Team A goes top down),
            // until overflowAmount is under the defined threshold.
            if(firstCalls > overflowAmount){
                List<Call__c> overflow_calls = 
                    [SELECT Id, Attempt__c, Status__c, Team__c, 
                     Locked__c, Earliest_Call_Time__c, 
                     Call_Template__c, Call_Pattern__c, Scheduled__c, 
                     Lead_Score__c, Prospect__c, Time_Zone__c
                     FROM Call__c 
                     WHERE Attempt__c =1 
                     AND Call__c.Locked__c != true 
                     AND RecordType.Name = 'Prospect Call'
                     AND Status__c != 'Completed' 
                     AND Time_Zone__c 
                     IN: tz_list
                     ORDER BY CreatedDate asc LIMIT 1];
                
                if(overflow_calls.size() > 0){
                    overflow_call = overflow_calls[0];
                    overflow_call.Locked__c = true;
                    overflow_call.Locked_Time__c = Datetime.now(); 
                    overflow_call.Locked_By__c = usr.Id;
                    update overflow_call;
                }
                
            }
        }
        //--END OVERFLOW CHECK--//
        
        
        if(last_15_min_call != null){
            System.debug('Returning \'last_15_min_call\'');
            return last_15_min_call;
        }
        else if (overflow_call != null){
            System.debug('Returning \'overflow_call\'');
            return overflow_call;
        }
        
        else{
            Call__c call = [SELECT Id, Attempt__c, Status__c,  Team__c, Locked__c, 
                            Earliest_Call_Time__c, Call_Template__c, 
                            Call_Pattern__c, Scheduled__c, Lead_Score__c, 
                            Prospect__c, Time_Zone__c 
                            FROM Call__c 
                            WHERE Call__c.Time_Zone__c IN :tz_list
                            AND Call__c.Team__c >= : usr_team
                            AND Call__c.Locked__c != true 
                            AND RecordType.Name = 'Prospect Call'
                            AND Status__c != 'Completed' 
                            AND DNC__c != true
                            AND Earliest_Call_Time__c <=: datetime.now() 
                            ORDER BY Scheduled__c desc, 
                            Team__c asc, Lead_Score__c desc, Attempt__c asc 
                            LIMIT 1];
            System.debug('getfirstResponseCall - call.Id >>' + call.Id);
            System.debug('getfirstResponseCall - call.Time_Zone__c >>' + 
                         call.Time_Zone__c);
            call.Locked__c = true;
            call.Locked_By__c = userInfo.getUserId();
            call.Locked_Time__c = Datetime.now();        	
            update call;
            
            System.debug('Returning standard call ' +
                         '(i.e. not last_15_min_call' + 
                         ' or overflow_call)');
            return call;
            
        }
        
    }
    
    
    @AuraEnabled
    public static Prospect__c getFirstResponseProspect(String prospectId) {       
        
        system.debug('****ProspectId: ' + prospectId);        
        Prospect__c callProspect = [SELECT Id, Name, First_Name__c, DNC__c, 
                                    Last_Name__c, Home_Phone__c, Work_Phone__c, 
                                    Mobile_SMS_Phone__c, Personal_Email__c, 
                                    State__c, Time_Zone_Lookup__c, Why__c, 
                                    Facts_about_Debt__c, Hope__c, Postal_Code__c 
                                    FROM Prospect__c 
                                    WHERE Id =: prospectId LIMIT 1];
        System.debug('****Time_Zone_Id: ' + callProspect.Time_Zone_Lookup__c);
        return callProspect;
    }
    
    
    @AuraEnabled
    public static void createNextCall(String prospectId, String callPatternId, 
                                      Integer attempt, String callTemplateId, String callbackDT, Id callId ){
                                          System.debug('*****JR***** callbackDT: ' + callbackDT);
                                          DateTime earliest;
                                          Boolean sched;
                                          System.debug('*****JR*****sched ' + sched);
                                          System.debug('*****JR*****earliest ' + earliest);
                                          System.debug('Entering createNextCall: prospectId >> ' + prospectId);
                                          System.debug('Entering createNextCall: callPatternId >> ' 
                                                       + callPatternId);
                                          System.debug('Entering createNextCall: attempt >> ' + attempt);
                                          System.debug('Entering createNextCall: callTemplateId >> ' 
                                                       + callTemplateId);
                                          System.debug('Entering createNextCall: callbackDT >> ' + callbackDT);
                                          
                                          //If this is a scheduled Callback, mark the call record as 'Contacted'
                                          if(callbackDT != null && callId != null){
                                               List<Call__c> calls = [SELECT Id, Contacted__c FROM Call__c WHERE Id =: callId];
                                                      Call__c call = calls[0];
                                                      call.Contacted__c = True;
                                                      System.debug('Scheduled CB: Call >> ' + call);
                                                      System.debug('Scheduled CB: Call.Contacted >> ' + call.Contacted__c);
                                                      update call;
                                          }
                                          
                                          //Query Call Template WHERE Call_Pattern = 
                                          //callPatternId and attempt = attempt+1
                                          
                                          System.debug('About to create new_attempt: attempt >> ' + attempt);
                                          Integer new_attempt;
                                          new_attempt = Integer.ValueOf(attempt) + 1;
                                          System.debug('Just created new_attempt: new_attempt >> ' + new_attempt);
                                          
                                          
                                          //Query size of List<Call_Template__c> 
                                          //WHERE Call_Pattern__c =: callPatternId
                                          //and compare to new_attempt
                                          List<Call_Template__c> templates = [SELECT Id
                                                                              FROM Call_Template__c
                                                                              WHERE Call_Pattern__c =: 
                                                                              callPatternId];
                                          
                                          if(new_attempt > templates.size()){
                                              Call_Pattern__c callPattern = [SELECT Unlimited_Calls__c 
                                                                             FROM Call_Pattern__c 
                                                                             WHERE Id =: callPatternId];
                                              System.debug('*****JR*****checking for ' + 
                                                           'callPattern.Unlimited_Calls__c to be true or false:  ' 
                                                           + callPattern.Unlimited_Calls__c);
                                              if (callPattern.Unlimited_Calls__c == true){
                                                  
                                                  Call_Template__c max_tmplt = [SELECT Id, 
                                                                                Min_Hours_Since_Previous_Call__c, 
                                                                                Team__c 
                                                                                FROM Call_Template__c
                                                                                WHERE Id =: callTemplateId];                           
                                                  System.debug('*****JR*****callbackDT variable check in ' + 
                                                               'unlimited calls = true branch:  ' + callbackDT);
                                                  if(callbackDT == null){
                                                      earliest = calculateEarliestCallTime(
                                                          max_tmplt.Min_Hours_Since_Previous_Call__c);
                                                      sched = false; 
                                                      System.debug('*****JR*****if callback is blank setting ' + 
                                                                   'earliest to a calculated value, earliest=  ' + earliest);
                                                      
                                                      System.debug('*****JR*****if callback is blank setting ' + 
                                                                   'sched to false sched=  ' + sched);
                                                  }else{
                                                      earliest = Datetime.parse(callbackDT);
                                                      sched = true; 
                                                      
                                                                                                           
                                                      System.debug('*****JR*****if callback is not blank ' +  
                                                                   'setting earliest to a callbackDT, ' + 
                                                                   ' earliest=  ' + earliest);
                                                      System.debug('*****JR*****if callback is not blank ' + 
                                                                   'setting sched to true sched=  ' + sched);
                                                  }
                                                  
                                                  createCallRecord(prospectId, callPatternId, max_tmplt.Id, 
                                                                   new_attempt, earliest, max_tmplt.Team__c, sched);
                                              }
                                              else{system.debug('******JR*****If callpattern is not ' + 
                                                                'unlimited calls flow breaks here');
                                                   // break 
                                                  }
                                              
                                          }
                                          
                                          else{
                                              Call_Template__c tmplt = [SELECT Id, 
                                                                        Min_Hours_Since_Previous_Call__c, Team__c 
                                                                        FROM Call_Template__c
                                                                        WHERE Call_Pattern__c =: callPatternId
                                                                        AND Attempt__c =: new_attempt];
                                              
                                              //Create Call record based on tmplt parameters 
                                              system.debug('******JR*****If tmplt is not Null excute ' + 
                                                           'following branch tmpt='+tmplt);
                                              system.debug('******JR*****If callbackDT is null execute ' + 
                                                           'first branch callbackDT='+callbackDT);
                                              if(callbackDT == null){
                                                  earliest = calculateEarliestCallTime(
                                                      tmplt.Min_Hours_Since_Previous_Call__c);
                                                  sched = false;
                                                  System.debug('*****JR*****if callback null,  setting ' + 
                                                               'earliest to a calculated value, earliest=  ' + earliest);
                                                  System.debug('*****JR*****if callback null, setting sched ' + 
                                                               'to false sched=  ' + sched);
                                              }
                                              else{
                                                  earliest = Datetime.parse(callbackDT);
                                                  sched = true;
                                                  System.debug('*****JR*****if callback NOT null,  setting ' + 
                                                               'earliest to a callbackDT, earliest=  ' + earliest);
                                                  System.debug('*****JR*****if callback NOT null, setting ' + 
                                                               'sched to true sched=  ' + sched);
                                              }
                                              
                                              createCallRecord(prospectId, callPatternId, tmplt.Id, 
                                                               new_attempt, earliest, tmplt.Team__c, sched);
                                              
                                          }
                                      }    
    
    
    
    
    
    
    
    
    @AuraEnabled
    public static void createCallRecord(String prospectId, String callPatternId, 
                                        String callTemplateId, Integer attempt, DateTime earliest, 
                                        String team, Boolean scheduled){
                                            //Looking for any existing open call records.
                                            List<Call__c> openCall = [SELECT Id, Name, Prospect__c
                                                                      FROM Call__c
                                                                      WHERE Prospect__c =: prospectId 
                                                                      AND RecordType.Name = 'Prospect Call'
                                                                      AND Status__c != 'Completed' 
                                                                      AND Locked__c != true 
                                                                     ];   
                                            //Check if another open call exists
                                            if(openCall.size() > 0){
                                                //break//
                                            }
                                            else{
                                                
                                                Call__c call = new Call__c();
                                                List<RecordType> RT_IDs = [SELECT Id FROM RecordType WHERE Name = 'Prospect Call' AND sObjectType = 'Call__c'];
										        Id RT_ID = RT_IDs[0].Id;
                                        		
                                                call.Prospect__c = prospectId;
                                                call.RecordTypeId = RT_ID;
                                                call.Call_Pattern__c = callPatternId;
                                                call.Call_Template__c = callTemplateId; 
                                                call.Attempt__c = attempt;
                                                call.Earliest_Call_Time__c = earliest;
                                                call.Team__c = team;
                                                call.Status__c = 'Not Started';
                                                call.Scheduled__c = scheduled;
                                                insert call;
                                            }  
                                        }
    
    @AuraEnabled
    public static DateTime calculateEarliestCallTime(Decimal minHours){
        //Takes in minimum hours until the next call and returns a 
        //dateTime representing Now() + minHours;
        
        //Convert minHours decimal into separate Hours and Minutes variables
        Integer minInt = minHours.intValue();
        Decimal minDec = minHours - minInt;
        Integer minMinutes = (60*minDec).round(
            System.RoundingMode.HALF_UP).intValue();
        
        Datetime rightNow = Datetime.now();
        Datetime earliest = rightNow.addHours(minInt);
        earliest = earliest.addMinutes(minMinutes);
        
        return earliest;
        
    }
    
    @AuraEnabled
    public static String getStateCBNumber(String prospectState){
        //Takes in State abbreviation from Prospect.State__c and returns the 
        //callback number stored on the State__c object that matches 
        //the abbreviation
        
        List<State__c> state = [SELECT Id, Name, Abbreviation__c, 
                                Outbound_CallerID__c
                                FROM State__c
                                WHERE Abbreviation__c =: prospectState
                                LIMIT 1];
        String callbackNumber = string.valueOf(state[0].Outbound_CallerID__c);
        
        return callbackNumber;
    }
}