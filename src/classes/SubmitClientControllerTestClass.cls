@isTest
private class SubmitClientControllerTestClass {
    
    private static List<Prospect__c> createProspects(Integer numProspects, String postal_code){                                
        //Accepts an integer for # of Prospects to creat and a String for the postal_code 
        //Creates TZ and Zip Code records for the provided postal_code as well as for '90210'
        Time_Zone__c tz = new Time_Zone__c(Name='America/Chicago', Standard_Offset__c = -6, DST__c = true);
        insert tz;
        if(postal_code != '12345'){
            Zip_Code__c z = new Zip_Code__c(Name=postal_code, Time_Zone__c = tz.Id);            
            insert z;
        }
        
        Time_Zone__c test_tz = new Time_Zone__c(Name='America/Los_Angeles', Standard_Offset__c = -8, DST__c = true);
        insert test_tz;
        Zip_Code__c test_z = new Zip_Code__c(Name='90210', Time_Zone__c=test_tz.Id);
        insert test_z;
        
        List<Prospect__c> prospects = new List<Prospect__c>(); 
        for (Integer i=0; i<numProspects; i++){
            Prospect__c p = new Prospect__c(Name='TestProspect'+i, Postal_Code__c = postal_code);
            prospects.add(p); 
        }
        
        insert prospects;
        return prospects;
    }
    
    private static List<Call__c> createCalls(List<Prospect__c> prospects){
        
        //Takes list of Propsects created by method above and creates Call records for each
        
        Call_Pattern__c cp = new Call_Pattern__c();
        cp.Active__c = true;
        cp.Name = 'Test_Call_Pattern';
        cp.Unlimited_Calls__c = true;
        cp.Calls__c = null;
        cp.Consults__c = null;
            
        insert cp;

        SMS_Template__c sms = new SMS_Template__c();
        sms.Name = 'Supercool';
        sms.SMS_Message__c = 'Lorem ipsum dolor sit amet';
		
        insert sms;
        
        Email_Template__c eml = new Email_Template__c();
        eml.Name = 'McLovin';
        eml.HTML_Email__c = 'Lorem ipsum dolor sit amet';
		
        insert eml;
        
        Call_Template__c ct = new Call_Template__c(); 
        ct.Call_Pattern__c = cp.Id;
        ct.Attempt__c = 1;
        ct.Min_Hours_Since_Previous_Call__c = 0;
        ct.Team__c = 'A';
        ct.SMS_Template__c = sms.Id;
        ct.Email_Template__c = eml.Id;
        insert ct;
        
        
        
        List<Call__c> calls = new List<Call__c>();
        for(Prospect__c p : prospects){
            Call__c c = new Call__c();
            c.Prospect__c = p.Id;
            c.Call_Pattern__c = cp.Id;
            c.Call_Template__c = ct.Id;
            c.Status__c = 'Not Started';
            c.Attempt__c = 1;
            c.Earliest_Call_Time__c = System.now();
            c.Team__c = 'A';
            calls.add(c);
        }
        insert calls;
        return calls;
    }
    
    @IsTest static void testProspectObjectInfoReturned(){
        List<Prospect__c> prsps = createProspects(1,'60090');
        
        test.startTest();
        Prospect__c pr = SubmitClientController.grabInfo(prsps[0].Id);
        test.stopTest();
        
        System.assertNotEquals(null, pr);
    }
    
    @IsTest static void testNewBankruptcyDealCreated(){
        
        List<Prospect__c> prsps = createProspects(1,'60090');
        System.debug('prsps ='+prsps);
        List<Call__c> calls = createCalls(prsps);
        System.debug('calls ='+calls);
        System.debug('calls[0].Prospect__c'+calls[0].Prospect__c);
        System.debug('prsps[0].Id='+prsps[0].Id);
        
        List<Call__c> relatedcall = [SELECT Id, Status__c, Call_Pattern__c, Prospect__c 
                                     FROM Call__c 
                                     WHERE Prospect__c =: prsps[0].Id 
                                     LIMIT 1];
        String why = 'because';
        String hope = 'home';
        String debts = 'some';
        String notes = 'scribbles';
        
        System.debug('Size of relatedcall List ='+relatedcall.size());
        
        test.startTest();
        System.debug('Right Before Test >>> prsps[0].Id='+prsps[0].Id);
        SubmitClientController.createDeal(prsps[0].Id, why, hope, debts, notes, relatedcall[0].Id);
        test.stopTest();
        List<Deal__c> deal = [SELECT Id, RecordTypeId, Name FROM Deal__c WHERE Prospect__c =: prsps[0].Id LIMIT 1];
        RecordType recType = [SELECT Id, Name FROM RecordType WHERE Name = 'Bankruptcy' AND SObjectType = 'Deal__c'];
        
        System.assertNotEquals(null, deal[0].Id);
        System.assertEquals(recType.Id, deal[0].RecordTypeId);
    }
    
    @IsTest static void testSubmittedForConsultTaskCreated(){
        List<Prospect__c> prsps = createProspects(1,'60090');
        List<Call__c> calls = createCalls(prsps);
        List<Call__c> relatedcall = [SELECT Id, Status__c, Call_Pattern__c, Prospect__c 
                                     FROM Call__c 
                                     WHERE Prospect__c =: prsps[0].Id 
                                     LIMIT 1];
        String why = 'because';
        String hope = 'home';
        String debts = 'some';
        String notes = 'scribbles';
        
        test.startTest();
        SubmitClientController.createDeal(prsps[0].Id, why, hope, debts, notes, relatedcall[0].Id);
        test.stopTest();
        
        List<Task> tsk = [SELECT Id, Subject
                          FROM Task
                          WHERE WhatId =:prsps[0].Id];
        
        System.assertNotEquals(null, tsk[0].Id);
        System.assertEquals(tsk[0].Subject, 'Submitted for Consult');
        
    }
    
    @IsTest static void testCallUpdatedToCompleted(){
        List<Prospect__c> prsps = createProspects(1,'60090');
        List<Call__c> calls = createCalls(prsps);
        List<Call__c> relatedcall = [SELECT Id, Call_Pattern__c, Prospect__c, Status__c 
                                     FROM Call__c 
                                     WHERE Prospect__c =: prsps[0].Id 
                                     LIMIT 1];
        String why = 'because';
        String hope = 'home';
        String debts = 'some';
        String notes = 'scribbles';
        
        test.startTest();
        SubmitClientController.createDeal(prsps[0].Id, why, hope, debts, notes, relatedcall[0].Id);
        test.stopTest();
        
        List<Call__c> postrelatedcall = [SELECT Id, Call_Pattern__c, Prospect__c, Status__c 
                                     FROM Call__c 
                                     WHERE Prospect__c =: prsps[0].Id 
                                     LIMIT 1];
        
        System.assertEquals('Completed',postrelatedcall[0].Status__c);
    }
    @IsTest static void testCallPatternCallsAndConsultsIncrementedWhenNull(){
        List<Prospect__c> prsps = createProspects(1,'60090');
        List<Call__c> calls = createCalls(prsps);
        List<Call__c> relatedcall = [SELECT Id, Status__c, Call_Pattern__c, Prospect__c 
                                     FROM Call__c 
                                     WHERE Prospect__c =: prsps[0].Id 
                                     LIMIT 1];
        Call_Pattern__c pattern = [SELECT Id, Calls__c, Consults__c 
                                   FROM Call_Pattern__c 
                                   WHERE Id =: relatedcall[0].Call_Pattern__c];
        String why = 'because';
        String hope = 'home';
        String debts = 'some';
        String notes = 'scribbles';
        
        test.startTest();
        SubmitClientController.createDeal(prsps[0].Id, why, hope, debts, notes, relatedcall[0].Id);
        test.stopTest();
        
        Call_Pattern__c testcp = [SELECT Id, Calls__c, Consults__c
                                 FROM Call_Pattern__c
                                 WHERE Id =: relatedcall[0].Call_Pattern__c
                                 LIMIT 1];
        
        System.assertEquals(1, testcp.Calls__c);
        System.assertEquals(1, testcp.Consults__c);

    }

    @IsTest static void testCallPatternCallsAndConsultsIncremented(){
        List<Prospect__c> prsps = createProspects(1,'60090');
        List<Call__c> calls = createCalls(prsps);
        List<Call__c> relatedcall = [SELECT Id, Status__c, Call_Pattern__c, Prospect__c 
                                     FROM Call__c 
                                     WHERE Prospect__c =: prsps[0].Id 
                                     LIMIT 1];
        Call_Pattern__c pattern = [SELECT Id, Calls__c, Consults__c 
                                   FROM Call_Pattern__c 
                                   WHERE Id =: relatedcall[0].Call_Pattern__c];
        String why = 'because';
        String hope = 'home';
        String debts = 'some';
        String notes = 'scribbles';
        
        pattern.Calls__c = 1;
        pattern.Consults__c = 1;
        update pattern;
        
        test.startTest();
        SubmitClientController.createDeal(prsps[0].Id, why, hope, debts, notes, relatedcall[0].Id);
        test.stopTest();
        
        Call_Pattern__c testcp = [SELECT Id, Calls__c, Consults__c
                                 FROM Call_Pattern__c
                                 WHERE Id =: relatedcall[0].Call_Pattern__c
                                 LIMIT 1];
        
        System.assertEquals(2, testcp.Calls__c);
        System.assertEquals(2, testcp.Consults__c);
    }
    
    
   // @IsTest static void testPostRequestResponseReturned(){
   //    String url = 'www.salesforce.com';
        
   //    test.startTest();
   //    String res = SubmitClientController.postReqToDPP(url);
   //    test.stopTest();
        
   //    System.assertNotEquals(null, res);
        
   //}
    
    
    
    
    
}