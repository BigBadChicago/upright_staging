public with sharing class PointsController {

    @AuraEnabled
    public static String fetchPoints() {
        String userId = UserInfo.getUserId();
        String base_query = 'SELECT SUM(CACT__Points__c ) FROM CACT__Achievement__c WHERE  CACT__User__c = :userId';
        String month_where = ' AND CACT__AssignmentDate__c >= THIS_MONTH';
        String week_where = ' AND CACT__AssignmentDate__c >= THIS_WEEK';
        String day_where = ' AND CACT__AssignmentDate__c >= TODAY';

		System.debug(base_query + month_where);
        
        Map<String, Object> pts;
        String JsonPts;
        
        List<AggregateResult> vals = Database.query(base_query);
		Object lifetime = vals[0].get('expr0');
        
        List<AggregateResult> month_vals = Database.query(base_query + month_where);
		Object month = month_vals[0].get('expr0');
        
        List<AggregateResult> week_vals = Database.query(base_query + week_where);
		Object week = week_vals[0].get('expr0');
        
        List<AggregateResult> day_vals = Database.query(base_query + day_where);
		Object day = day_vals[0].get('expr0');
        
        
        System.debug('Points>>> Day: ' + day_vals[0].get('expr0'));
        
        pts = new Map<String, Object>();
		pts.put('lifetime', lifetime);
        pts.put('month', month);
        pts.put('week', week);
        pts.put('day', day);
		JsonPts = JSON.serialize(pts);		

        System.debug('>>> ' + JsonPts);
		        
        return JsonPts;
        
       
    }

}