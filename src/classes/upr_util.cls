public without sharing class upr_util {
    //This is a global utility class that can be called upon from any Lightning App's
    //server-side apex to perform common or repetitive tasks.
    
    public static void createActivity(String assignee, String subject, String whatId,
                                      String taskType, String status, String descrip,
                                      Date taskDate, String callId, String caseTaskId,
                                      String whoId){
                                          Task t = new Task();
                                          t.OwnerId = assignee;
                                          t.Subject = subject;
                                          t.WhatId = whatId;
                                          t.Type = taskType;
                                          t.Status = status;
                                          t.Description = descrip;
                                          t.ActivityDate = taskDate;
                                          t.Call__c = callId;
                                          t.Case_Task__c = caseTaskId;
                                          t.WhoId = whoId;
                                          insert t;
                                      }
}