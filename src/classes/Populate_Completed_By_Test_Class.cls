@isTest
private class Populate_Completed_By_Test_Class {
	
	public static List<Case_Task__c> createCaseTasks(Integer numCaseTasks, Boolean isCompleted){
	// Create Case File with Name and Stage
	
		List<Case_File__c> cfs = new List<Case_File__c>(); 
		for (Integer i=0; i<numCaseTasks; i++){
			Case_File__c cf = new Case_File__c(Name='TestCaseFile'+i, Stage__c = 'Welcome Call');
			cfs.add(cf); 
		}
		insert cfs;

	// Create Case Task with Name and Case File ID
		List<Case_Task__c> cts = new List<Case_Task__c>(); 
		for (Integer j=0; j<numCaseTasks; j++){
			Case_File__c caseFile = cfs[j];
			//For each Case File inserted, add a Case Task
			if (isCompleted = true){
				Case_Task__c ct = new Case_Task__c(Name='TestCaseTask'+j, Case_File__c=caseFile.Id, Status__c = 'Completed');
				cts.add(ct);
			}
			else{
				Case_Task__c ct = new Case_Task__c(Name='TestCaseTask'+j, Case_File__c=caseFile.Id, Status__c = 'Not Started');
				cts.add(ct);	
			}
		}
		insert cts;	
		return cts;
	}
	
	@isTest static void insertOneCompletedCaseTask() {
		// Insert Completed Case Task
		List<Case_Task__c> caseTasks = createCaseTasks(1, true);
		
		// Assert that only 1 Case Task is returned
		System.assertEquals(1, caseTasks.size());

		for(Case_Task__c c: caseTasks){
			// Assert that Completed By is NOT null
			System.assertNotEquals(UserInfo.getUserId(), c.Completed_By__c);
		}
	}
	
	@isTest static void insertOneNotCompletedCaseTask() {
		// Insert Incomplete Case Task
		List<Case_Task__c> caseTasks = createCaseTasks(1, false);
		
		// Assert that only 1 Case Task is returned
		System.assertEquals(1, caseTasks.size());

		for(Case_Task__c c: caseTasks){
			// Assert that Completed By IS null
			System.assertEquals(null, c.Completed_By__c);
			
			//Update Case Task Status to 'Completed'
			c.Status__c = 'Completed';
			update c;

			//Verify that Completed By was populated 
			
			System.assertNotEquals(UserInfo.getUserId(), c.Completed_By__c);

		}
	}


	//@isTest static void insertFiveHundredCompletedCaseTasks() {
	//	// Insert 500 Case Tasks
	//	List<Case_Task__c> caseTasks = createCaseTasks(500, false);
		
	//	// Assert that 500 Case Tasks are returned
	//	System.assertEquals(500, caseTasks.size());

	//	// Assert that Completed By IS null
	//	for(Case_Task__c c: caseTasks){
	//		// Assert that Completed By is NOT null
	//		System.assertNotEquals(UserInfo.getUserId(), c.Completed_By__c);
	//	}		
	
	//}
}