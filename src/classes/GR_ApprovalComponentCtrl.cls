public class GR_ApprovalComponentCtrl {

    @AuraEnabled
    public static void getProcessApproval(String currentCaseFileId, String attyComments, Boolean isApproved) {
        // First, get the IDs for the WorkItems 
        System.debug('****JB**** THIS IS THE CASE FILE ID: ' + currentCaseFileId);
        System.debug('****JB**** THESE ARE THE ATTORNEY COMMENTS: ' + attyComments);
        System.debug('***JB***  THIS IS THE APPROVAL BOOLEAN: ' + isApproved );
        List<Case_Task__c> complianceCaseTaskList = [SELECT Id, Name, Case_File__c, RecordType.Name, Partner_Attorney__c, Status__c, Approval_Status__c FROM Case_Task__c WHERE Name = 'Review Intake' AND Case_File__c =: currentCaseFileId AND RecordType.Name = 'Review Intake' AND Status__c = 'In Progress' AND Approval_Status__c = 'Pending Partner Attorney Approval' ];
        Case_Task__c complianceCaseTask = complianceCaseTaskList[0];
        
        
        
        List<Id> approvalWorkItemIds = new List<Id>();
        for (List<ProcessInstance> pis : [Select (Select Id From Workitems) From ProcessInstance p WHERE p.TargetObjectId = :complianceCaseTask.Id AND p.Status = 'Pending']) {
            for (ProcessInstance pi : pis) {
                for (List<ProcessInstanceWorkitem> wis : pi.Workitems) {
                    for (ProcessInstanceWorkitem wi : wis ) {
                        approvalWorkItemIds.add(wi.id);
                    }
                }           
            }
        }
 
        // Instantiate the new ProcessWorkitemRequest object and populate it
        Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
       if (isApproved) {
            req2.setComments(attyComments);
            req2.setAction('Approve');
        } else {
            req2.setComments(attyComments);
            req2.setAction('Reject');
        }
        req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
 
        // Use the ID from the newly created item to specify the item to be worked
        req2.setWorkitemId(approvalWorkItemIds.get(0));
 
        // Submit the request for approval
        Approval.ProcessResult result2 =  Approval.process(req2);
 
        // Verify the results
        System.assert(result2.isSuccess(), 'Result Status:'+result2.isSuccess());
        
        //Create variable to hold specific approvl determination string
        String appStatus;
        if (isApproved){
            appStatus = 'Approved';
        } else {
            appStatus = 'Rejected';
        }
        //Convert Review Intake case task Id into a string
        String ctId = complianceCaseTask.Id;
        
        //Call helper utility to create Compliance Determination activity
        upr_util.createActivity(UserInfo.getUserId(),'GR Partner Intro Call Completed - '+appStatus, currentCaseFileId, 'Call', 'Completed', attyComments, Date.today(),null,ctId,null);
 
        // if (isApproved) {
        //     System.assertEquals('Approved', result2.getInstanceStatus(), 'Instance Status'+result2.getInstanceStatus());
        // } else {
        //     System.assertEquals('Rejected', result2.getInstanceStatus(), 'Instance Status'+result2.getInstanceStatus());
        // }
    }

    @AuraEnabled
    public static void getCallStatusToCompleted(String currentCaseFileId, String currentCallId, String attyComments, String appSessionId ) {
        Call__c curCall = [SELECT Id, Name, Status__c, Case_File__r.Id, Case_File__c, Completed__c, Completed_By__c FROM Call__c WHERE Id =: currentCallId AND Case_File__r.Id =: currentCaseFileId LIMIT 1];
        curCall.Status__c = 'Completed';
        curCall.Completed__c = DateTime.Now();
        curCall.Completed_By__c = userInfo.getUserId();
        update curCall;
        upr_util.createActivity(UserInfo.getUserId(),'GR Call Completed', currentCaseFileId, 'Call', 'Completed', attyComments, Date.today(),currentCallId,null,null);
        
        //Updating App Session with +1 on calls completed and updating last action occured datetime field
        List<App_Session__c> appSessions = [SELECT Id, Name, Last_Action_Occurred__c, Call_Completed__c FROM App_Session__c WHERE Id=:appSessionId];
        App_Session__c appSession = appSessions[0];
        appSession.Call_Completed__c = appSession.Call_Completed__c+1;
        appSession.Last_Action_Occurred__c = dateTime.now();
        update appSession;
    }
        
    @AuraEnabled
    public static void updateEarliestCallbackTime(String currentCaseFileId, String currentCallId, String earliestCallbackTimeString, String appSessionId ) {
        Call__c curCall = [SELECT Id, Name, Status__c, Case_File__r.Id, Attempt__c, Case_File__c, Earliest_Call_Time__c, Scheduled__c, Completed__c, Completed_By__c FROM Call__c WHERE Id =: currentCallId AND Case_File__r.Id =: currentCaseFileId LIMIT 1];
        curCall.Scheduled__c = true; 
        curCall.Earliest_Call_Time__c = Datetime.parse(earliestCallbackTimeString);
        Integer attempt = Integer.ValueOf(curCall.Attempt__c);
        curCall.Attempt__c = attempt + 1;
        update curCall;
        upr_util.createActivity(UserInfo.getUserId(),'GR Call Completed - Follow Up Scheduled', currentCaseFileId, 'Call', 'Completed', 'Scheduled follow up date: '+earliestCallbackTimeString, Date.today(),currentCallId,null,null);

        //Updating just App Session's last action occured datetime field
        List<App_Session__c> appSessions = [SELECT Id, Name, Last_Action_Occurred__c FROM App_Session__c WHERE Id=:appSessionId];
        App_Session__c appSession = appSessions[0];
        appSession.Last_Action_Occurred__c = dateTime.now();
        update appSession;
    }



}