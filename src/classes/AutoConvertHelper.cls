public with sharing class AutoConvertHelper {
//Methods providing additional logic for AutoConvertToProspectAndDeal.apxt

    
    public static List<Case_File__c> checkForCaseFiles(String lastName, 
            String email, String phone, String zipCode, String state){
        //searches for an existing Case_File__c record based on last name, 
            //email, and phone
                
        List<Case_File__c> caseFiles = [SELECT Id, Primary_Contact_Last_Name__c,
                                        Primary_Contact_ID__c,
                                        Primary_Contact_Email__c, 
                                        Primary_Contact_Phone__c
                                        FROM Case_File__c
                                        WHERE 
                                        (Primary_Contact_Last_Name__c
                                        =: lastName
                                        AND Primary_Contact_Email__c 
                                        =: email)];
                                        //Only matching on last name and 
                                        //email to err on the side of *not* 
                                        //creating a duplicate Case_File__c
                                        //and instead having somebody review 
                                        // the ticket.
        return caseFiles;
        
    }

    public static void createSupportTicket(String caseFileId, 
            String primaryContactId, String primaryEmail){
            //Creates Support Ticket notifying support team that a 
                //client is trying to reach us
            Ticket__c t = new Ticket__c();
                t.Ticket_Name_Unabridged__c = 'Existing Client Requested Consultation';
                t.Status__c = 'New';
                t.Type__c = 'Support';
                t.Case_File__c = caseFileId;
                t.Client_Requestor__c = primaryContactId;
                t.Name = 'Client Requested Contact';
                t.Description__c = 'This client filled out a request online ' +
                'looking for bankruptcy information. Please contact them and ' +
                'see how we can be of service! \n\n' +
                
                'This ticket was created automatically when the client ' +
                'submitted their request. Client was matched on last name ' +
                'and email. If you feel this was an error, please send an ' + 
                'email with the link to sfdc@uprightlaw.com \n\n';

                insert t;
        
    }

    public static List<Prospect__c> checkForDupeProspects(String lastName, 
            String email, String phone){
            
        List<Prospect__c> dupes =   [SELECT Last_Name__c, Personal_Email__c, 
                                    Home_Phone__c, Mobile_SMS_Phone__c,
                                    Work_Phone__c
        							 FROM Prospect__c
        							 WHERE Personal_Email__c =: email
        							 AND Name != 'Phone Lead'
        							 AND Last_Name__c =: lastName
        							 AND((Home_Phone__c =: phone)
        							 	OR (Mobile_SMS_Phone__c =: phone)
        							 	OR(Work_Phone__c =: phone))
        							 ORDER BY CreatedDate desc];
	 	return dupes;
    }
    
    public static List<Deal__c> checkForDeals(List<Id> prospectIds){
//[TODO] ----->     Filter for record type.Name = Bankruptcy
//[TODO] ----->     Filter for Deal Owner != Leads Needing Consult queue
        List<Deal__c> openDeals = [SELECT Id, Stage__c, OwnerId
                                    FROM Deal__c 
                                    WHERE Prospect__c IN: prospectIds
                                    AND Stage__c != 'Hired'
                                    ORDER BY CreatedDate desc];
                                    
        return openDeals;
    }

    
    public static void sendEmailToDealOwner(String dealId, String ownerId){
        String BASE_URL = URL.getSalesforceBaseUrl().toExternalForm();  //'http://upright.my.salesforce.com/';
        User dealOwner = [SELECT Name, Email, FirstName
                            FROM User
                            WHERE Id =: ownerId
                            LIMIT 1];
                            
        String fromAddress = 'sfdc@uprightlaw.com';
        String subject = 'Deal Follow Up';
        String msgBody = 'This Prospect for this Deal recently submitted ' +
                            'another request for a BK Consultation. Please ' +
                            'review the details of the Deal and follow up ' +
                            'with the Prospect. \n\n' +
                            
                            BASE_URL + '/' + dealId + '\n\n' +
                            
                            fromAddress;

        Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
            msg.SetSubject(subject);
            msg.setReplyTo(fromAddress);
            msg.setToAddresses(new String[] {dealOwner.Email});
            msg.setPlainTextBody(msgBody);
        
        Messaging.SingleEmailMessage[] messages = 
            new Messaging.SingleEmailMessage[]{ msg };
    
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if (results[0].isSuccess()) {
            System.debug('AutoConvertHelper.cls >> Successfully sent ' +
            'email to Deal Owner regarding duplicate Prospect__c');
             
        } 
        else {
            System.debug('AutoConvertHelper.cls >> Failed sending email ' +
            'to Deal owner regarding dupe Prospect__c');
        }
    }
    
 
   public static List<Call__c> checkForCalls(List<Id> prospectIds){             
    //
    List<Call__c> calls = [SELECT Id, Status__c, Attempt__c, 
                            Earliest_Call_Time__c, Call_Pattern__c, Prospect__c
                            FROM Call__c
                            WHERE Prospect__c IN : prospectIds AND RecordType.Name = 'Prospect Call'
                            ORDER BY Earliest_Call_Time__c desc];
    return calls;
        
    }
    
    public static Boolean checkDNC(Id prospectId){
        List<Prospect__c> prospects = [SELECT DNC__c 
                                        FROM Prospect__c
                                        WHERE Id =: prospectId
                                        LIMIT 1];
        Boolean dnc = prospects[0].DNC__c;
        
        return dnc;
    }
    
    
    public static void flipDNC(Id prospectId){
        List<Prospect__c> DNCdupes = [SELECT Id, DNC__c
                                        FROM Prospect__c
                                        WHERE Id =: prospectId
                                        LIMIT 1];
        Prospect__c p = DNCdupes[0];
        p.DNC__c = false;
        
        update p;
    }
    
    public static void createCall(Id prospectId, Id callPatternId, Decimal attempt){
        //Check if Attempt exceeds number of calls on call pattern
        //If Attempt exceeds # of calls, get template for max call
        //but keep attempt # as-is
        
        Call__c call = new Call__c();
        Call_Template__c template = new Call_Template__c();
        List<RecordType> RT_IDs = [SELECT Id FROM RecordType WHERE Name = 'Prospect Call' AND sObjectType = 'Call__c'];
        Id RT_ID = RT_IDs[0].Id;
        
        List<Call_Template__c> templates = 
                                    [SELECT Id, Attempt__c, SMS_Template__c, 
                                    Email_Template__c, Team__c
                                    FROM Call_Template__c
                                    WHERE Call_Pattern__c =: callPatternId
                                    ORDER BY Attempt__c desc];
        
        //If attempt > number of calls in pattern                            
        if(attempt > templates.size()){
            template = templates[0];    
        }
        else{
            template = [SELECT Id, Attempt__c, SMS_Template__c, 
                        Email_Template__c, Team__c
                        FROM Call_Template__c
                        WHERE Call_Pattern__c =: callPatternId
                        AND Attempt__c =: attempt];
        }
        
            call.Prospect__c = prospectId; 
        	call.RecordTypeId = RT_ID;
            call.Call_Pattern__c = callPatternId;
            call.Call_Template__c = template.Id;
            call.Attempt__c = template.Attempt__c;
            call.Status__c = 'Not Started';
            call.SMS_Template__c = template.SMS_Template__c;
            call.Email_Template__c = template.Email_Template__c;
            call.Team__c = template.Team__c;
            call.Scheduled__c = true;
            call.Earliest_Call_Time__c = System.now();
           
           insert call; 
    }
    
    @future
    public static void incrementCallPatternProspects( Id patternId, 
                                                        Double prospects){
        Call_Pattern__c pattern = [SELECT Id, Prospects__c 
                                    FROM Call_Pattern__c 
                                    WHERE Id =: patternId];
        pattern.Prospects__c = prospects;
        update pattern;
    }    
}