@isTest
public class upr_twilioTaskRemoteJS_Test {
	@testSetup static void setupTestData(){
        RecordType acctRecType = [SELECT Id, Name, DeveloperName, SobjectType FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Consumer' LIMIT 1];
        RecordType cfRecType = [SELECT Id, Name, DeveloperName, SobjectType FROM RecordType WHERE SobjectType = 'Case_File__c' AND DeveloperName = 'Chapter_7' LIMIT 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='Case Manager']; 
      
        List<User> usrList = new List<User>();
        for( Integer i = 0; i < 6; i++ ){
            User u = new User(
                Alias = 'testUsr' + i, 
                Email='testUsr' + i + '@testorg.com', 
                EmailEncodingKey='UTF-8', 
                LastName='Testing' + i, 
                LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', 
                ProfileId = p.Id, 
                TimeZoneSidKey='America/Los_Angeles',   
                UserName='testUsr' + i + '@UprightLawtestorg.com');
            usrList.add(u);
        }
        insert usrList; 

        State__c testState = new State__c(Name = 'Illinois', State_Entity__c = 'UpRight Law LLC', Case_Manager__c = usrList[3].Id, Chapter_13_Internal_Attorney__c = usrList[4].Id, Chapter_7_Internal_Attorney__c = usrList[5].Id ,Abbreviation__c = 'XX', Outbound_CallerID__c = '3125551212');
        insert testState;
        List<Account> accounts = new List<Account>();
        for( Integer i = 0; i < 2; i++ ){
            accounts.add(new Account(Name = 'Test Acc' + i, BillingState = 'XX', RecordTypeId = acctRecType.Id));
        }
        insert accounts;
        List<Contact> contacts = new List<Contact>();
        for( Account acc:accounts ){
            for( Integer i = 0; i < 3; i++){
                contacts.add(new Contact(
                    FirstName = 'First',
                    LastName  = 'Last' + i, 
                    AccountId = acc.Id,  
                    Phone = '(555) 555+555' + i,
                    Email = 'test' + i + '@testemail.com'
                ));
            }
        }
        insert contacts;
        List<Case_File__c> caseFiles = new List<Case_File__c>();
 //       for(Contact con : contacts ){
            caseFiles.add(new Case_File__c(
                RecordTypeId = cfRecType.Id,
                Single_or_Joint_Filing__c = 'Single',
                Marital_Status__c = 'Single',
                BK_District__c = 'Illinois - Central',
                Name = contacts[0].FirstName + ' ' + contacts[0].LastName + ' ' + cfRecType.Name,
                Primary_Account__c = contacts[0].AccountId,
                Primary_Contact__c = contacts[0].Id,
                Case_Manager__c = usrList[0].Id,
                Internal_Attorney__c = usrList[1].Id,
                Sr_Case_Manager__c = usrList[2].Id,
                Stage__c = 'Welcome Call'
                ));
 //       }
        insert caseFiles;
    }   

	
	static testMethod void test_method_one() {
		List<Case_File__c> testCaseFiles = [SELECT Id, Name, Stage__c FROM Case_File__c WHERE Stage__c = 'Welcome Call'];
		PageReference pageRef = Page.upr_twilioOutboundCallPage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', testCaseFiles[0].Id);
      	upr_twilioOutboundCallCustomCtrl controller = new upr_twilioOutboundCallCustomCtrl();
      	upr_twilioTaskRemoteJS taskRemote = new upr_twilioTaskRemoteJS(controller);
      	// THESE ARE TEST CREDENTIALS FROM TWILIO!!!!
        // THEY ARE USED SO UNIT TESTS DO NOT COUNT AGAINST THE COST FOR
        // CALLS AND OTHER USAGE
        String ACCOUNT_SID = 'ACe35fa95cdf61c8bb333d19b6d74c7c30';
		String AUTH_TOKEN = '30026fb2937f1a83808d0fea3f7d7cdd';
		TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
		Map<String,String> params = new Map<String,String>();
        params.put('To','13129401222');
        params.put('From' ,'15005550006');
        params.put('Url', 'http://foo.com/call.xml');
        System.debug('Twilio Client: ' + client);
        System.debug('Twilio Account: ' + client.getAccount());
		//TwilioCall testCall = client.getAccount().getCalls().create(params);
		//System.debug(testCall.getDuration());
		//System.debug(testCall.getSid());  
        //upr_twilioTaskRemoteJS.createCallTask(testCaseFiles[0].Id, 'TEST TEST TEST', testCall.getSid());
    }
	
    static testMethod void test_method_two() {
		List<Case_File__c> testCaseFiles = [SELECT Id, Name, Stage__c FROM Case_File__c WHERE Stage__c = 'Welcome Call'];
		PageReference pageRef = Page.upr_twilioOutboundCallPage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', testCaseFiles[0].Id);
        ApexPages.StandardController stdCtlr = new ApexPages.StandardController(testCaseFiles[0]);
   		upr_twilioTaskRemoteJS taskRemote = new upr_twilioTaskRemoteJS(stdCtlr);
   		// THESE ARE TEST CREDENTIALS FROM TWILIO!!!!
        // THEY ARE USED SO UNIT TESTS DO NOT COUNT AGAINST THE COST FOR
        // CALLS AND OTHER USAGE
        String ACCOUNT_SID = 'ACe35fa95cdf61c8bb333d19b6d74c7c30';
		String AUTH_TOKEN = '30026fb2937f1a83808d0fea3f7d7cdd';
		TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
		Map<String,String> params = new Map<String,String>
        {
            'To'   => '+13129401222',
            'From' => '+15005550006',
            'Url'  => 'http://demo.twilio.com/docs/voice.xml'
        };
        //params.put('To','13129401222');
        //params.put('From' , '15005550006');
        //params.put('Url', 'http://demo.twilio.com/docs/voice.xml');
        System.debug('Twilio Client: ' + client);
        System.debug('Twilio Account: ' + client.getAccount());
        System.debug('Twilio Calls: ' + client.getAccount().getCalls());
  //      TwilioCall testCall = client.getAccount().getCalls().create(params);
		//System.debug(testCall.getDuration());
		//System.debug(testCall.getSid());
  //    	upr_twilioTaskRemoteJS.createCallTask(testCaseFiles[0].Id, 'TEST TEST TEST', testCall.getSid());
     
	}
	
}