public with sharing class TwilioSoftphoneCtrl {
    public TwilioAccount twiAccount { get; set; }
    public TwilioCapability capability;
    public String fromPhoneNumber { get; set; }
    public String clientPhoneNumber { get; set; }  
    public String twilioCallSID { get; set; }

    @AuraEnabled
    public static TwilioAccount getTwilioAccount(){
        TwilioAccount ta = TwilioAPI.getDefaultAccount();
        return ta;
    }
    
    @AuraEnabled
    public static String getTwilioToken() { 
      	TwilioCapability capability = TwilioAPI.createCapability();
        capability.allowClientOutgoing(TwilioAPI.getTwilioConfig().ApplicationSid__c);  
        return capability.generateToken(); 
    } 
    
    @AuraEnabled
    public static String getStatePhoneNumber(String prospectState) { 
      	List<State__c> prospectStateList  = [SELECT Id, Name, Abbreviation__c, Outbound_CallerID__c, State_SMS_Number__c FROM State__c WHERE Abbreviation__c = :prospectState];
        return prospectStateList[0].Outbound_CallerID__c;
    } 
    
    @AuraEnabled
    public static string makeTwilioCall(String prospectState, String outgoingPhoneNumber) { 
        TwilioRestClient client = TwilioAPI.getDefaultClient();
        User curUser = [SELECT Id, Name, Phone FROM User WHERE Id =: System.UserInfo.getUserId() LIMIT 1];
        String userPhone = curUser.Phone;
      	List<State__c> prospectStateList  = [SELECT Id, Name, Abbreviation__c, Outbound_CallerID__c, State_SMS_Number__c FROM State__c WHERE Abbreviation__c = :prospectState];
        String FromPhoneNumber = prospectStateList[0].Outbound_CallerID__c;
        String baseUrl = System.URL.getSalesforceBaseURL().toExternalForm();
        String fromEncoded = EncodingUtil.urlEncode(FromPhoneNumber, 'UTF-8');
        String toEncoded = EncodingUtil.urlEncode(outgoingPhoneNumber, 'UTF-8');
        
		Map<String, String> params = new Map<String, String>{
            'From' => FromPhoneNumber,
            'To' => userPhone,
            'Url' => 'http://upright.force.com/twilioOutboundCallXML?toNum=' + toEncoded + '&fromNum=' + fromEncoded,
            'IfMachine' => 'Continue',
            'Timeout' => '225'
        };
        TwilioCall call = client.getAccount().getCalls().create(params); 
        return call.getSid();
    } 
    
    @AuraEnabled
    public static void endTwilioCall(String callSID, String toNumber) { 
        TwilioRestClient client = TwilioAPI.getDefaultClient();
        Map<String,String> filters = new Map<String,String>{'Status'=>'in-progress', 'Sid'=>callSID, 'To'=>toNumber};
		Iterator<TwilioCall> calls = client.getAccount().getCalls(filters).iterator();
        while (calls.hasNext()) {
    		TwilioCall call = calls.next();
 			String parentCallID = call.getParentCallSid();
            TwilioCall parentCall = client.getAccount().getCall(parentCallID);
			parentCall.hangup();	
        }
    }
	
    @AuraEnabled
    public static void startTwilioConference(String callSID, String prosLastName) { 
        TwilioCall clientCall;
        String clientSID;
        String confRoomName = prosLastName + String.valueOf(Date.today().month()) + String.valueOf(Date.today().day()) + String.valueOf(Date.today().year());
        TwilioRestClient client = TwilioAPI.getDefaultClient();
        TwilioCall agentCall = client.getAccount().getCall(callSID); 
        Map<String,String> filters = new Map<String,String>{'Status'=>'in-progress', 'ParentCallSid'=>callSID};
	    Iterator<TwilioCall> calls = client.getAccount().getCalls(filters).iterator();
        while (calls.hasNext()) {
    		TwilioCall call = calls.next();
 			clientSID = call.getSID();
        } 
        clientCall = client.getAccount().getCall(clientSID); 
        Map<String,String> properties = new Map<String,String>{
            'Url'=> 'http://upright.force.com/twilioConferenceXML',
            'Method' => 'POST'
   		};
		clientCall.updateResource(properties);
        agentCall.updateResource(properties);
	}
    
    @AuraEnabled
    public static void holdTwilioCall(String callSID) { 
        TwilioRestClient client = TwilioAPI.getDefaultClient();
        TwilioCall call = client.getAccount().getCall(callSID); 
        Map<String,String> properties = new Map<String,String>{
            'Url'=> 'https://twimlets.com/holdmusic?Bucket=com.twilio.music.classical',
            'Method' => 'POST'
   		};
		call.updateResource(properties);
	} 
    
    @AuraEnabled
    public static void holdTwilioChildCall(String callSID) { 
        TwilioRestClient client = TwilioAPI.getDefaultClient();
        Map<String,String> filters = new Map<String,String>{'Status'=>'in-progress', 'ParentCallSid'=>callSID};
		Iterator<TwilioCall> calls = client.getAccount().getCalls(filters).iterator();
        while (calls.hasNext()) {
    		TwilioCall call = calls.next();
 			Map<String,String> properties = new Map<String,String>{
            	'Url'=> 'https://twimlets.com/holdmusic?Bucket=com.twilio.music.classical',
            	'Method' => 'POST'
   			};
			call.updateResource(properties);
        } 
 	} 
    
	@AuraEnabled
    public static void unholdTwilioCall(String callSID) { 
        TwilioRestClient client = TwilioAPI.getDefaultClient();
        TwilioCall call = client.getAccount().getCall(callSID); 
        call.redirect('http://uprightlaw-twilio1.herokuapp.com/call', 'POST');  
              
    } 
    
    public String getTwimlForOutboundCall() {
        String toNumber = '';
        String fromNumber = '';
        toNumber = System.currentPageReference().getParameters().get('toNum');
		fromNumber = System.currentPageReference().getParameters().get('fromNum');
		TwilioTwiML.Response res = new TwilioTwiML.Response();
        TwilioTwiML.Dial d = new TwilioTwiML.Dial(toNumber);
	    d.setCallerId(fromNumber);
        d.setTimeout(240);
    	res.append(d);
    	return res.toXML();
  	}
    
    public String getTwimlForConference() {
        String agentLastName = System.UserInfo.getLastName();
        String confRoomName = agentLastName + String.valueOf(Date.today().month()) + String.valueOf(Date.today().day()) + String.valueOf(Date.today().year());
        TwilioTwiML.Response r = new TwilioTwiML.Response();
        TwilioTwiML.Dial d = new TwilioTwiML.Dial();
        TwilioTwiML.Conference c = new TwilioTwiML.Conference(confRoomName);
        c.setStartConferenceOnEnter(true); 
        c.setEndConferenceOnExit(true); 
        c.setMuted(false);
        c.setBeep(false);
        d.append(c);
        r.append(d);
        return r.toXML();   
  	}
    
    
    
    
    
}