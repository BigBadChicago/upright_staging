global class SchedulableDocketEntriesRefresh 
			implements Schedulable, Database.Batchable<sObject>
			, Database.AllowsCallouts, Database.Stateful {
//	USAGE:
//
//	SchedulableDocketEntriesRefresh sder = new SchedulableDocketEntriesRefresh();
//	// Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
//	String scheduleStr = '0 39 * * * ?';	// every 60 minutes
//	String jobID = system.schedule( 'Refresh Docket Entries', scheduleStr, sder );

	// implementing Database.Batchable
	global static ID batchProcessID;
	global List<String> errorList;
	public static String sfdcHostname {
		get {
			if( sfdcHostname == null ) {
				sfdcHostname = 'https://' + System.URL.getSalesforceBaseUrl().getHost().remove('-api' ) + '/';
			}
			return sfdcHostname;
		}
		set;
	}
	public static Boolean updateCases = true;
	public static final String TEST_URL = 'https://www.inforuptcy.com/rss/test_12345';

	global Database.QueryLocator start( Database.BatchableContext BC ) {
		return startBatch();
	}

	global Database.QueryLocator startBatch() {
		errorList = new List<String>();
		String theQuery = 'SELECT ID, Name, Case_Number__c, Long_Case_Number__c, '
							+ 'RSS_URL__c, BK_District__c '
						+ 'FROM Case_File__c '
						+ 'WHERE Stage__c NOT IN ( \'Discharged\', \'Dismissed\' ) '
							+ 'AND Filed_Date__c != null '
							+ 'AND BK_District__c != null '
							+ 'AND Case_Number__c != null ';
							//+ 'AND RSS_URL__c != null ';
		return Database.getQueryLocator( theQuery );
	}

	global void execute( Database.BatchableContext BC, List<Case_File__c> scope ) {
		executeBatch( scope );
	}

	global void executeBatch( List<Case_File__c> scope ) {
		List<Docket_Entry__c> newDocketEntryList = new List<Docket_Entry__c>();

		// collect cases with missing RSS URL
		Set<String> courtSet = new Set<String>();
		List<Case_File__c> casesWithoutCodeList = new List<Case_File__c>();
		for( Case_File__c aCase : scope ) {
			// fetch RSS for a case file
			String url = aCase.RSS_URL__c;
			if( url != null ) {
				continue;
			}

			// collect court codes from cases with null RSS
			courtSet.add( aCase.BK_District__c );
			casesWithoutCodeList.add( aCase );
		}

		// obtain the RSS feed URLs that are missing
		List<Case_File__c> caseList;
		if( courtSet.size() > 0 ) {
			// find each URL and save them into the respective cases
			updateCases = false;
			caseList = setCourtCodesAndFindFeeds( 
									courtSet, casesWithoutCodeList, errorList );

			// get the docket entries for the cases that didn't have a RSS URL
			for( Case_File__c aFile : caseList ) {
				if( aFile.RSS_URL__c == null ) {
					continue;
				}

				addDocketEntries( aFile.ID, aFile.RSS_URL__c, newDocketEntryList );
			}
		}

		for( Case_File__c aCase : scope ) {
			// fetch RSS for a case file
			String url = aCase.RSS_URL__c;
			if( url == null ) {
				// case files with null RSS have been handled in the previous loop
				continue;
			}
			String fileID = aCase.ID;

			addDocketEntries( fileID, url, newDocketEntryList );
		}

		// save docket entries
		if( newDocketEntryList.size() > 0 ) {
			saveDocketEntries( newDocketEntryList, errorList );
		}

		// update cases with the RSS feed links found
		if( caseList != null && caseList.size() > 0 ) {
			saveCaseFiles( caseList, errorList );
		}
	}

	public class FindFeedParameters {
		public String fileID;
		public String findFeedURL;
		public List<String> nameList;
	}

	public static List<Case_File__c> setCourtCodesAndFindFeeds( 
						Set<String> courtSet, List<Case_File__c> newCaseFileList
						, List<String> errorList ) {

		// retrieve and create map of district names to court codes
system.debug( courtSet );
		List<Bankruptcy_Court__c> courtList = [ SELECT ID, BK_District__c, Court_Code__c 
												FROM Bankruptcy_Court__c 
												WHERE BK_District__c IN :courtSet 
												ORDER BY BK_District__c ];
		Map<String, String> courtToCodeMap = new Map<String, String>();
		for( Bankruptcy_Court__c aCourt : courtList ) {
			courtToCodeMap.put( aCourt.BK_District__c, aCourt.Court_Code__c );
		}

		// prepare list of parameters
		List<FindFeedParameters> parameterList = new List<FindFeedParameters>();

		// create URL to find the RSS feed
		for( Case_File__c aFile : newCaseFileList ) {
			// collect the names of the parties involved in the case
			String[] unverifiedNameList = new String[] {};
			if( aFile.Name != null ) {
				unverifiedNameList = aFile.Name.split( ' ' );
			}
			List<String> nameList = new List<String>();
			for( String aName : unverifiedNameList ) {
				if( aName.length() > 2 && aName != 'Chapter' ) {
					nameList.add( aName );
				}
			}

			// NOTE: it couldn't get the names via account lookup
			// we had to use the case file name (above)

			// build the URL to find the RSS feed
system.debug( courtToCodeMap );
			String courtCode = courtToCodeMap.get( aFile.BK_District__c );
			if( courtCode == null ) {
				errorList.add( 'Case file ID ' + sfdcHostname + aFile.ID 
					+ ' - Could not find court code for BK District = ' 
					+ aFile.BK_District__c );
system.debug( 'Case file ID ' + sfdcHostname + aFile.ID 
					+ ' - Could not find court code for BK District = ' 
					+ aFile.BK_District__c );
				continue;
			}

			// use case number in format 99-99999
			String caseNumber = aFile.Case_Number__c;
			if( aFile.Long_Case_Number__c != null ) {
				// use long case number when available to override parsing
				caseNumber = aFile.Long_Case_Number__c;
				if( caseNumber.contains( ':' ) ) {
					caseNumber = caseNumber.replace( ':', '-' );
				}
			} else {
				if( caseNumber == null ) {
					//errorList.add( 'Case file ID ' + sfdcHostname + aFile.ID 
					//	+ ' - Case number is null' );
					continue;
				}
				caseNumber = caseNumber.replace( '-bk-', '-' ).trim();
				if( caseNumber.contains( ':' ) ) {
					caseNumber = caseNumber.substringAfter( ':' );
				}
				if( caseNumber.endsWith( '-7' ) ) {
					caseNumber = caseNumber.substringBeforeLast( '-7' );
				}

				if( caseNumber.length() != 8 || caseNumber.indexOf( '-' ) != 2 ) {
					errorList.add( 'Case file ID ' + sfdcHostname + aFile.ID 
						+ ' - Case number ' + caseNumber + ' seems incorrect' );
				}
			}

			String findFeedURL = 'https://www.inforuptcy.com/rss/find-feed?court_code=' 
						+ courtCode + '&case_no=' + caseNumber;
system.debug( findFeedURL );
			FindFeedParameters aParameter = new FindFeedParameters();
			aParameter.fileID = aFile.ID;
			aParameter.findFeedURL = findFeedURL;
			aParameter.nameList = nameList;
			parameterList.add( aParameter );
		}

		// submit a single call passing a parameter containing all find feed requests
		List<Case_File__c> caseList = new List<Case_File__c>();
		if( parameterList.size() > 0 ) {
			if( system.isBatch() || system.isFuture() || system.isScheduled() ) {
				caseList = findRSSFeedURLs( parameterList, errorList );
			} else {
				String JSONlist = JSON.serialize( parameterList );
				findRSSFeedURLsAtFuture( JSONlist );
			}
		}

		return caseList;
	}

	@future(callout=true)
	public static void findRSSFeedURLsAtFuture( String JSONlist ) {
		List<FindFeedParameters> parameterList = (List<FindFeedParameters>) 
						JSON.deserialize( JSONlist, List<FindFeedParameters>.class );
		List<String> errorList = new List<String>();
		findRSSFeedURLs( parameterList, errorList );
	}
	public static List<Case_File__c> findRSSFeedURLs( 
					List<FindFeedParameters> parameterList, List<String> errorList ) {
		List<Case_File__c> caseList = new List<Case_File__c>();

		// for each of the cases specified in parameters, find the RSS URL
		for( FindFeedParameters aParameter : parameterList ) {
			Case_File__c aCase = getRSSFeedURL( aParameter.fileID
						, aParameter.nameList, aParameter.findFeedURL, errorList );
			if( aCase != null ) {
				caseList.add( aCase );
			}
		}

		// update the case files with the RSS URL
		if( caseList.size() > 0 && updateCases == true ) {
			update caseList;
		}

		return caseList;
	}

	public static Case_File__c getRSSFeedURL( String fileID, List<String> nameList
				, String findFeedURL, List<String> errorList ) {
		// obtain JSON containing RSS feed
		HttpRequest req = new HttpRequest();
		req.setEndpoint( findFeedURL );
		req.setMethod( 'GET' );
		
		String response = '';
		Http h = new Http();
		
		if ( ! Test.isRunningTest() ) { 
			HttpResponse res = h.send( req );
			response = res.getBody();
		} else {
			// dummy response to facilitate testing
			response = '[{"case_no":"1:12-bk-12345","case_title":"Test","rss_feed":"' + TEST_URL + '"}]';
		}
system.debug( response );

		List<findFeed> feedList = (List<findFeed>) JSON.deserialize( response
															, List<findFeed>.class );
		if( feedList.size() <= 0 ) {
			errorList.add( 'Case file ID ' + sfdcHostname + fileID 
					+ ' - No response from ' + findFeedURL );
			return null;
		}

		for( findFeed aFeed : feedList ) {
			// determine whether the case title has names that match the parties involved
			Boolean namesMatch = false;
			if( nameList.size() > 1 ) {
				// only attempt to match names if there is at least one name to match
				for( String aName : nameList ) {
					if( aFeed.case_title != null && aFeed.case_title.containsIgnoreCase( aName ) ) {
						namesMatch = true;
						break;
					}
				}
			} else {
				// if list is empty, don't restrict and just get the first case
				namesMatch = true;
			}

			// the names matched and the feed was found
			if( namesMatch ) {
				return getCaseFileWithRSSFeedURL( fileID, aFeed.rss_feed );
			}
		}

		errorList.add( 'Case file ID ' + sfdcHostname + fileID 
				+ ' - Could not find feed with names matching ' + nameList 
				+ ' in ' + findFeedURL );

		return null;
	}

	public static Case_File__c getCaseFileWithRSSFeedURL( String fileID, String feedURL ) {
		Case_File__c aCase = new Case_File__c();
		aCase.ID = fileID;
		aCase.RSS_URL__c = feedURL;
		return aCase;
	}

	public class findFeed {
		public String case_no;
		public String case_title;
		public String rss_feed;
	}

	global static void addDocketEntries( String fileID, String url
					, List<Docket_Entry__c> newDocketEntryList ) {
		RSS.channel chan = RSS.getRSSData( url );

		// ALTERNATIVE:  query case file and get number of the latest docket loaded then load only newer ones
		if( chan.items.size() <= 0 ) {
			return;
		}

		for( RSS.item anItem : chan.items ) {
			// create docket entry for each item in the RSS
			Docket_Entry__c d = new Docket_Entry__c();
			d.Name = anItem.title.left( 80 );
			d.Title__c = anItem.title;
			d.Guid__c = anItem.guid;
			d.Link__c = anItem.link;
			d.Description__c = anItem.description;
			d.Publication_Date__c = anItem.getPublishedDateTime();
			d.Case_File__c = fileID;
			newDocketEntryList.add( d );
		}
	}

	global static List<String> saveDocketEntries( List<Docket_Entry__c> newDocketEntryList
								, List<String> errorList ) {
		// upsert using GUID to avoid duplicates
		Database.UpsertResult[] aSaveResultList = Database.upsert( newDocketEntryList
												, Schema.Docket_Entry__c.fields.Guid__c, false );

		Integer i = -1;
		for( Database.UpsertResult aSaveResult : aSaveResultList ) {
			i ++;
			if( aSaveResult.success ) {
				continue;
			}

			String errorMessages = ConcatenateErrors( aSaveResult.errors );
			if( errorMessages != '' ) {
				errorList.add( 'Record: ' + newDocketEntryList[ i ].Guid__c 
							+ ' - ' + errorMessages );
			}
		}

		return errorList;
	}

	global static List<String> saveCaseFiles( List<Case_File__c> caseList
								, List<String> errorList ) {
		// upsert using GUID to avoid duplicates
		Database.SaveResult[] aSaveResultList = Database.update( caseList, false );

		Integer i = -1;
		for( Database.SaveResult aSaveResult : aSaveResultList ) {
			i ++;
			if( aSaveResult.success ) {
				continue;
			}

			String errorMessages = ConcatenateErrors( aSaveResult.errors );
			if( errorMessages != '' ) {
				errorList.add( 'Record: ' + caseList[ i ].Name 
							+ ' - ' + errorMessages );
			}
		}

		return errorList;
	}

	global static String ConcatenateErrors( Database.Error[] errors ) {
		String errorMessages = '';
		for( Database.Error anError : errors ) {
			// ignore duplicate errors
			if( anError.message.contains( 'Duplicate external id specified' ) ) {
				continue;
			}
			errorMessages = errorMessages + ' / ' + anError.message;
		}
		return errorMessages;
	}

	global void finish( Database.BatchableContext BC ) {
		finishBatch();
	}
	global void finishBatch() {
		if( errorList != null && errorList.size() > 0 ) {
			// save errors to the log for later troubleshooting - a0L prefix
			String errorMessages = 'Case File RSS Update - Errors: \n' 
						+ String.join( errorList, '\n' );

			Integer errorSize = errorMessages.length();
			List<Error_Log__c> errorLogList = new List<Error_Log__c>();
			do {
				Error_Log__c aLog = new Error_Log__c();
				aLog.Errors__c = errorMessages.left( 32000 );
				errorLogList.add( aLog );

				errorSize = errorSize - 32000;
				if( errorSize > 0 ) {
					errorMessages = errorMessages.substring( 32000, 32000 + errorSize );
				}
			} while( errorSize > 0 );

			insert errorLogList;
		}
	}

	// implementing Schedulable
	global void execute( SchedulableContext sc ) {
		// start the batch execution 
		batchProcessID = Database.executeBatch( this, 25 );

		//refreshDocketEntries();
	}

}