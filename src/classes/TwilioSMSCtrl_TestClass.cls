@IsTest
private class TwilioSMSCtrl_TestClass {
    @testSetup static void setupTestData(){
    	RecordType prsRecType = [SELECT Id, Name, DeveloperName, SobjectType FROM RecordType WHERE SobjectType = 'Prospect__c' AND DeveloperName = 'Bankruptcy' LIMIT 1];
    	
        Profile p = [SELECT Id FROM Profile WHERE Name='Case Manager']; 
      	// CREATING TEST USERS
        List<User> usrList = new List<User>();
        for( Integer i = 0; i < 6; i++ ){
            User u = new User(
                Alias = 'testUsr' + i, 
                Email='testUsr' + i + '@testorg.com', 
                EmailEncodingKey='UTF-8', 
                LastName='Testing' + i, 
                LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', 
                ProfileId = p.Id, 
                TimeZoneSidKey='America/Chicago',   
                UserName='testUsr' + i + '@uprightlaw.testorg.com');
                System.debug('This user is :' + u);
            usrList.add(u);
        }
        insert usrList; 
		// CREATING TEST STATES
    	State__c testState = new State__c(Name = 'Illinois', State_Entity__c = 'UpRight Law LLC', Case_Manager__c = usrList[3].Id, Chapter_13_Internal_Attorney__c = usrList[4].Id, Chapter_7_Internal_Attorney__c = usrList[5].Id ,Abbreviation__c = 'XX', Outbound_CallerID__c = '3125551212');
        insert testState;
        // CREATING TEST TIME ZONES
        Time_Zone__c tz = new Time_Zone__c(Name='America/Chicago', Standard_Offset__c = -6, DST__c = true);
	    insert tz;
        Time_Zone__c test_tz = new Time_Zone__c(Name='America/Los_Angeles', Standard_Offset__c = -8, DST__c = true);
		insert test_tz;
        // CREATING TEST ZIP CODES
        Zip_Code__c test_z1 = new Zip_Code__c(Name='60056', Time_Zone__c=tz.Id);
        insert test_z1;
        Zip_Code__c test_z2 = new Zip_Code__c(Name='90210', Time_Zone__c=test_tz.Id);
        insert test_z2;
        
        SMS_Template__c testDisclaimerSMS = new SMS_Template__c( Name = 'Disclaimer', SMS_Message__c = 'This is the test diclaimer text.');
        insert testDisclaimerSMS;
        SMS_Template__c testResponseSMS = new SMS_Template__c( Name = 'Response', SMS_Message__c = 'This is the test Response text.');
        insert testResponseSMS;
        SMS_Template__c testSMSMessage1 = new SMS_Template__c( Name = 'TestMessage1', SMS_Message__c = 'This is the first test message text.');
        insert testSMSMessage1;
        Call_Pattern__c testCallPattern = new Call_Pattern__c( Name = 'testCallPattern', Active__c = true);
        insert testCallPattern;
        Call_Template__c testCallTemplate = new Call_Template__c( Attempt__c = 1, Call_Pattern__c = testCallPattern.Id, Team__c = 'A', SMS_Template__c = testSMSMessage1.Id );
        insert testCallTemplate;
        // SMS_Template__c testDisclaimerSMS = new SMS_Template__c( Name = 'Disclaimer', SMS_Message__c = 'This is the test diclaimer text.');
        // insert testDisclaimerSMS;
        
        // CREATING TEST PROSPECTS
        List<Prospect__c> prospects = new List<Prospect__c>();
        for( Integer i = 0; i < 2; i++ ){
            prospects.add(new Prospect__c(
                Name = 'Test Prospect' + i,
                First_Name__c = 'Test',
                Last_Name__c = 'Prospect'+i,
                Home_Phone__c = '123-123-1234',
                Work_Phone__c = '987-987-9876',
                Mobile_SMS_Phone__c = '454-454-4545',
                Personal_Email__c = 'TestProspect' + i + '@testorg.com',
                Street__c = '1910 N Elm Street',
                City__c = 'Springfield',
                Postal_Code__c = '60056',
                State__c = 'XX', 
                SMS_Disclaimer_Sent__c = false,
                DNC_SMS__c = false,
                RecordTypeId = prsRecType.Id));
        }
        Prospect__c noMobileNumberProspect = new Prospect__c(
                Name = 'LandLine Prospect',
                First_Name__c = 'LandLine',
                Last_Name__c = 'Prospect',
                Home_Phone__c = '123-103-1234',
                Work_Phone__c = '987-717-9876',
                //Mobile_SMS_Phone__c = '454-454-4545',
                Personal_Email__c = 'LandLineProspect@testorg.com',
                Street__c = '1910 N Elm Street',
                City__c = 'Springfield',
                Postal_Code__c = '60056',
                State__c = 'XX', 
                SMS_Disclaimer_Sent__c = false,
                DNC_SMS__c = false,
                RecordTypeId = prsRecType.Id);
        prospects.add(noMobileNumberProspect);
        Prospect__c uniqueMobileNumberProspect = new Prospect__c(
                Name = 'Mobile Prospect',
                First_Name__c = 'Mobile',
                Last_Name__c = 'Prospect',
                Home_Phone__c = '765-765-7654',
                Work_Phone__c = '987-987-9876',
                Mobile_SMS_Phone__c = '(234) 234-2345',
                Personal_Email__c = 'MobileProspect@testorg.com',
                Street__c = '1910 N Elm Street',
                City__c = 'Springfield',
                Postal_Code__c = '60056',
                State__c = 'XX', 
                SMS_Disclaimer_Sent__c = true,
                DNC_SMS__c = false,
                RecordTypeId = prsRecType.Id);
        prospects.add(uniqueMobileNumberProspect);
        insert prospects;
        // CREATING TEST CALLS
        List<Call__c> calls = new List<Call__c>();
        for( Prospect__c prspt : prospects ){
            for( Integer i = 0; i < 3; i++){
                calls.add(new Call__c(
                  	Status__c = 'Not Started',
                    Attempt__c  = i+1, 
                    Prospect__c = prspt.Id,  
                    Team__c = 'A'
                ));
            }
        }
        insert calls;
       
    }
    
    static testMethod void testSendSmsMessage() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGen());
        
        TwilioSMSCtrl controller = new TwilioSMSCtrl();
        Prospect__c testProspect = [SELECT Id, Name, DNC__c, First_Name__c, Mobile_SMS_Phone__c, Home_Phone__c, Zip_Code_Lookup__c, DNC_SMS__c, SMS_Disclaimer_Sent__c, Zip_Code_Lookup__r.SMS_Phone_Number__c FROM Prospect__c WHERE Name = 'Test Prospect0' LIMIT 1];
        Call__c testCall = [SELECT Id, Name, DNC__c, Attempt__c, Prospect__c, Prospect__r.Id, Call_Template__c, Call_Template__r.SMS_Template__c, Call_Template__r.SMS_Template__r.SMS_Message__c FROM Call__c WHERE Prospect__c =: testProspect.Id AND Attempt__c = 1 LIMIT 1];
        String smsMessageSent = controller.sendProspectTextMessage(testCall.Id, testProspect.Id);
        System.assertEquals('0', smsMessageSent);
        
    }
    
    
    static testMethod void testIncomingSMSMessage() {
        PageReference pageRef = Page.twilioInboundSMSXML;
        Test.setCurrentPage(pageRef);
        // pageRef.getParameters().put('From', '12342342345');
        // pageRef.getParameters().put('To', '12175551451');
        // pageRef.getParameters().put('Body', 'This is a test message.');
      
        ApexPages.currentPage().getParameters().put('From', '+12342342345');
        ApexPages.currentPage().getParameters().put('To', '12175551451');
        ApexPages.currentPage().getParameters().put('Body', 'This is a test message.');
        TwilioSMSCtrl controller = new TwilioSMSCtrl();
        String fromNumberParam = ApexPages.currentPage().getParameters().get('From');
        String formatedFromPhone = controller.formatPhone(fromNumberParam);
        System.assertEquals(formatedFromPhone, '(234) 234-2345');
        PageReference newPR = controller.saveIncomingText();
    
        
        
    }
    
    // static testMethod void testSmsMessage() {
    //     SendTextMessageController.SmsMessage message = new SendTextMessageController.SmsMessage(TO_NUM, FROM_NUM, BODY);
    //     String postResult = message.formatForPostReq();
        
    //     system.assert(postResult.contains('To=' + TO_NUM.replaceAll('[^\\d]', '')), 'Did not contain the right To number');
    //     system.assert(postResult.contains('From=' + FROM_NUM.replaceAll('[^\\d]', '')), 'Did not contain the right From number');
    //     system.assert(postResult.contains('Body=' + BODY), 'Did not contain the right To number');
    // }
    
    
    
    
    
    
    
    
    
    
    
    

}